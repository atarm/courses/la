#!/usr/bin/env bash

function demo_infinite_loop() {
    while true; do
        :
    done

    while :; do
        :
    done
}
