#!/usr/bin/env bash

# -u Treat unset variables as an error when substituting
set -u
set -o nounset

# -e  Exit immediately if a command exits with a non-zero status.
set -e
set -o errexit

# -o pipefail the return value of a pipeline is the status of\
#             the last command to exit with a non-zero status,\
#             or zero if no command exited with a non-zero status
set -o pipefail