#!/usr/bin/env bash

function demo_var_sub_def() {
    exist_var=has_value
    empty_var=""
    def_val="here i am..."
    echo ${exist_var:-"${def_val}"}
    echo ${unexist_var:-"${def_val}"}
    echo ${empty_var:-"${def_val}"}
}

function demo_literal_str() {
    n=101
    str1=this_is_a_long_string_without_quotation$n str2="this is a \"double quoted\" string\" $n"
    str3='this is a single quoted string $n'

    echo $str1
    echo $str2
    echo $str3
}

demo_literal_str

demo_var_sub_def
