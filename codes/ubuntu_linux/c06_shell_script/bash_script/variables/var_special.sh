#!/usr/bin/env bash

function demo_at_and_asterisk( ) {

    echo 'directly print $*==>'"$*"
    echo 'directly print $@==>'"$@"

    echo 'print "$*" in for loop...'
    for val in "$*"; do
        echo -en "$val\t"
    done

    echo
    echo $#

    echo 'print "$@" in for loop...'
    i=0
    for val in "$@"; do
        echo -en "$((++i))==>$val\t"
    done
}

demo_at_and_asterisk a b c d