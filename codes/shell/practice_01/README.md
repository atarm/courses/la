# Shell Practice

original ==> [bjdzliu. Linux’ Shell 编程题](https://bjdzliu.com/2019/11/08/linux_shell/)

## 使用for循环在/test目录下通过随机小写10个字母加固定字符串test批量创建10个html文件

[sample](./generate_random.sh)

## 将以上文件名中的test全部改成oldgirl(用for循环实现)，并且html改成大写

[sample 01](./rename_1.sh)

[sample 02](./rename_2.sh)

## 批量创建10个系统帐号test01-test10并设置密码（密码为随机8位字符串）

[sample](./create_users.sh)

## 写一个脚本，实现判断10.0.0.0/24网络里，当前在线用户的IP有哪些

[sample](./ping.sh)

## 写一个脚本解决DOS攻击生产案例

提示：

1. 根据web日志或者或者网络连接数，监控当某个IP并发连接数或者短时内PV达到100，即调用防火墙命令封掉对应的IP，监控频率每隔3分钟
1. 防火墙命令为：`iptables -A INPUT -s 10.0.1.10 -j DROP`

[sample](./blockip.sh)

## 开发shell脚本分别实现以脚本传参以及read读入的方式比较2个整数大小，以屏幕输出的方式提醒用户比较结果

注意：一共是开发2个脚本。当用脚本传参以及read读入的方式需要对变量是否为数字、并且传参个数做判断

[sample getopts](./sum_1.sh)

[sample read](./compare_2.sh)

## 打印选择菜单，一键安装Web服务

```bash {.line-numbers}
1.[install lamp]
2.[install lnmp]
3.[exit]

please input the num you want:
```

要求：

1. 当用户输入1时，输出“startinstalling lamp.”然后执行/server/scripts/lamp.sh，脚本内容输出”lamp is installed”后退出脚本；
1. 当用户输入2时，输出“startinstalling lnmp.” 然后执行/server/scripts/lnmp.sh输出”lnmp is installed”后退出脚本;
1. 当输入3时，退出当前菜单及脚本；
1. 当输入任何其它字符，给出提示“Input error”后退出脚本。
1. 要对执行的脚本进行相关条件判断，例如：脚本是否存在，是否可执行等。

[sample](./menu.sh)

## 间隔1分钟，持续监控web服务和db服务是否正常

[sample](./monitor_web.sh)

## 监控web站点目录（/var/html/www）下所有文件是否被恶意篡改（文件内容被改了），如果有就打印改动的文件名（发邮件），定时任务每3分钟执行一次

[sample](./monitor_web_dir.sh)

## 实现一个抓阄脚本

要求：

1. 执行脚本后，想去的同学输入英文名字全拼，产生随机数01-99之间的数字，数字越大就去参加项目实践，前面已经抓到的数字，下次不能在出现相同数字。
2. 第一个输入名字后，屏幕输出信息，并将名字和数字记录到文件里，程序不能退出继续等待别的学生输入

[sample](./chooseauser.sh)

## 使用for循环打印下面这句话中字母数不大于6的单词

`I am test teacher welcome to test training class.`

[sample](./charlength.sh)
