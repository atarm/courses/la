# 《Linux系统应用》

## About The Contents关于内容

以下课程或书籍与本仓库内容基本相同或具有大部分内容重叠：

1. 《Linux入门》
1. 《Linux应用》
1. 《Linux运维基础》
1. 《Linux编程基础》

以下课程或书籍是本仓库内容的深入或细化：

1. 《Linux系统管理》
1. 《Linux自动化运维》

以下课程或书籍与本仓库内容具有较大的差别：

1. 《Linux编程》
1. 《Linux环境高级编程》

## Conventions约定

1. `UNIX`和`Linux`发行版的名称统一使用全小写，以下情况例外
    1. 引用他人的部分
    1. `Linux`和`UNIX`作为单独的、完整的名称

## Tools工具

<!-- 1. [Fish shell playground.](https://rootnroll.com/d/fish-shell/) -->
2. [iximiuz labs.](https://labs.iximiuz.com/): daily free 2 hours playtime
3. [linux terminal online. tutorialspoint.](https://www.tutorialspoint.com/linux_terminal_online.php)

## Bibliographies参考文献

1. 👑👍📖（美）Brain W. Kernighan著, 韩磊 译, 陈硕 审校. 2021-03. _UNIX: A History and a Memoir(UNIX传奇：历史与回忆)_. 中国工信出版集团, 北京, 人民邮电出版社 北京.
1. 张金石 主编, 钟小平 汪健 副主编. Ubuntu Linux操作系统(第2版 微课版) [M]. 北京: 中国工信出版集团, 北京: 人民邮电出版社, 2020-06.
1. 🌟✨（美）W. Richard Stevens, Stephen A. Rago, 戚正伟 张亚英 尤晋元 译. 2019-10. _Advanced Programming in the UNIX Environment **(APUE)** , Third Edition(UNIX环境高级编程（第3版）)_. 北京: 中国工信出版集团, 北京: 人民邮电出版社. 978-7-115-51675-6.
1. 🌟✨（美）Brain W. Kernighan, Rob Pike著, 陈向群 等译. 1999-10. _The UNIX Programming Environment **(TUPE)**_. 机械工业出版社, 北京.
1. 🌟✨（美）Mendel Cooper. Advanced Bash-Scripting Guide **(ABS)** [DB/OL]. <https://tldp.org/LDP/abs/html/index.html> .2014-03-10.
1. 🌟（美）Mendel Cooper著, LinuxStory 译. 《高级Bash脚本编程指南》Revision 10中文版[DB/OL]. <https://github.com/LinuxStory/Advanced-Bash-Scripting-Guide-in-Chinese>.
1. 🌟（美）Mendel Cooper著, 杨春敏 黄毅 译. 《高级Bash脚本编程指南》中文版（Revision 3.9.1）[DB/OL]. <https://www.reddragonfly.org/abscn/index.html>. 2006-05-26.
1. 🌟✨Free Software Foundation. GNU Bash Manual[DB/OL]. <https://www.gnu.org/software/bash/manual/>. 2020-12-21.
1. （美）Richard Blum, Christine Bresnahan著, 门佳 武海峰 译. Linux Command and Shell Scripting Bible, 3E(Linux命令行与shell脚本编程大全（第3版）) [M]. 北京: 中国工信出版集团, 北京: 人民邮电出版社, 2016-08. ISBN: 978-7-115-42967-4.
1. （美）David Both著, 卢涛 李颖 译. The Linux Philosophy for SysAdmins: And Everyone Who Wants To Be One(Linux哲学) [M]. 北京: 机械工业出版社, 2019-09.
1. Glenn. GNU/Linux 点滴[DB/OL]. <https://gnu-linux.readthedocs.io/zh/latest/>, 2021-08-15.
1. 🌟 阮一峰. Bash 脚本教程[DB/OL]. <https://wangdoc.com/bash/index.html>, 2021-08-15.
1. 🌟 阮一峰. 阮一峰的网络日志[DB/OL]. <https://www.ruanyifeng.com/blog/>, 2021-10-10.
1. 🌟 ✨ TinyLab. Shell编程范例[DB/OL]. <https://github.com/tinyclub/open-shell-book>.
2016-08-30.
1. 🌟 ✨ Arch Linux. ArchWiki[DB/OL]. <https://wiki.archlinux.org/>.
1. SOCS(McGill's School of Computer Science). _SOCS Inside-Out_[DB/OL]. <https://www.cs.mcgill.ca/~guide/welcome.html>, 2002-09-06.
1. Mark Burgess. _The Unix Programming Environment_[DB/OL]. <http://markburgess.org/unix/unix_toc.html>. 2001-08.
1. _TCCM-course: linux/bash 2.0.1 documentation_[DB/OL]. <https://quantchem.kuleuven.be/unix_manual/index.html>.
1. 👍 💻 🔬 蓝桥云课. Linux基础入门[DB/OL]. <https://www.lanqiao.cn/courses/1>
