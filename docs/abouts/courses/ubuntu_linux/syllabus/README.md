# 教学大纲

## 基本信息

1. 课程名称：Linux系统应用
1. 课程编码：0822292
1. 课程类型：专业课程
1. 课程性质：选修
1. 适用范围：计算机科学与技术、软件工程、数据科学与大数据技术
1. 课程学分：2
1. 课程学时：36
1. 实验/实践学时：18
1. 课外学时：0
1. 考核方式：考查（平时40%+期末60%）

## 理论课程教学大纲

### 课程简介

`Linux`在`server os`中占有不可替代的地位，熟悉使用`Linux`是`system operator系统运维员`/`system administrator系统管理员`的必备技能、也是企业应用系统开发、云计算开发、大数据开发、人工智能开发等各类开发人员的重要基础技能。

课程以`end-user最终用户`、`system operator系统运维员`/`system administrator系统管理员`的视角学习`Linux`的相关应用和使用知识，本课程着重为目标为软件开发人员（含系统分析师、系统架构师、软件工程师、测试工程师）、软件系统运维人员、软件系统管理人员的学生培养基础的`Linux`技能以及知识体系（以便学生日后自行扩展深入学习）。

课程内容主要包话`Linux`概述、命令行及命令使用、`shell`脚本编程三大部分，基本不涉及`Linux Server`应用过程中较少使用的`GUI`图形界面。

### 教学目标

通过本课程的学习，要求学生达到下列目标：

1. 理解`Operating System Principle`与本课程的关系
1. 了解`UNIX-like`、`GNU/Linux`的历史及重要人物
1. 理解`UNIX-like`与`Windows`的联系与区别
1. 掌握`GNU/Linux`的基本使用、基本配置与管理、基本命令
1. 掌握`Shell`的基本编程
1. 掌握`GNU/Linux`的基本运维技能
1. 了解`GNU/Linux`的开发环境

### 主要教学模式和教学手段

本课程是一门理论和实践并重的课程，只注重理论、不注重实践，将无法学会使用`Linux`，不注重理论、只注重实践，将无法有效使用`Linux`，因此，本课程将采用理论与实践相结合、案例的教学方法，在理论上，通过案例引入概念、原理和方法，在实践上，通过实验巩固和延伸理论知识、并使用启发式教学方法，启发学生思考分析实验、组织相关讨论，充分调动学生的主观学习兴趣，以达到本课程的教学目的。

本课程要求充分利用Internet资源，发挥学生学习的主动性和利用网络资源的积极性，搜集最新的资料、了解最新的技术。

### 教学内容

#### Meeting Ubuntu遇见Ubuntu

1. `Linux`的安装
1. `Linux`概念：
    1. `UNIX`，`UNIX-like`，`POSIX`，`SUS`，`MINIX`，`GNU/Linux`
    1. :star2: `Linux Package Manager` and `Linux Distribution`
1. `Linux`使用环境
    1. :star2: `CLI`, `GUI` and `shell`
    1. `dpkg`, `apt` and `snap`
1. `Linux`编程环境
    1. 编辑器`vim`，`emacs`与`nano`
    1. 编译器`GCC`与调试器`GDB`
    1. 自动化构建工具`make`
    1. 自动化构建工具集`GNU Autotools`
1. `Linux GUI frameworks`: `GTK` and `Qt`

#### User Management用户管理

1. :star2: `user` and `group`用户与用户组
1. :star2: `user` and `group` management by command用户与用户组管理

#### File Management文件管理

1. :star2: `FHS`文件系统层次结构标准
1. :star2: file type文件类型
1. :star2: file management文件管理

#### Storage Management存储管理

1. :star2: storage overview存储概述：设备文件、磁盘分区、文件系统
1. disk partition and mount硬盘分区与挂载
1. :star2: logical volume manager逻辑卷管理
1. mount and use extern storage外部存储设备挂载和使用
1. data backup数据备份

#### System and Service Management系统及服务管理

1. :star2: `process` management进程管理
1. :star2: `system` and `service` management系统和服务管理
1. `jobs` schedule作业调度
1. `log` management日志管理

#### Shell Script脚本编程

1. :star2: script execute environment脚本运行环境
1. :star2: datatype, structure and variable数据类型、数据结构与变量
1. :star2: statement, expression and operator语句、表达式与运算符
1. :star2: flow control流程控制
1. :star2: function and file函数与文件

### 教学重点难点

1. 教学重点：`UNIX`、`UNIX-like`、`Linux`的历史和演变过程，命令行的交互式使用，`shell`脚本编程
1. 教学难点：命令的使用，`shell`的特殊语法

### 课内实验（实训）教学内容及要求

#### LA-E01：Meeting Ubuntu遇见Ubuntu

1. 实验性质：综合性
1. 实验学时：4
1. 实验目的：
    1. 掌握`Ubuntu`的安装
    1. 熟悉`Ubuntu`的虚拟控制台
    1. 理解`shell`命令
    1. 掌握`SSH`连接`Ubuntu`
    1. 了解`Vim`
1. 实验内容与要求：
    1. 实验内容01：安装`Ubuntu Server`
    1. 实验内容02：在多个虚拟控制台切换
    1. 实验内容03：通过`SSH`连接`Ubuntu`
    1. 实验内容04：使用`vim`新建编辑一个文本文件并保存
1. 实验条件：
    1. 系统环境：`Desktop`(`macOS`或`Windows`或`Linux`)或`Server`
    1. 软件环境：`VirtualBox`或其他虚拟机

#### LA-E02：Operations in Ubuntu/Ubuntu运维管理

1. 实验性质：综合性
1. 实验学时：6
1. 实验目的：
    1. 掌握`Ubuntu`的用户与组管理
    1. 掌握`Ubuntu`的文件与目录管理
    1. 掌握`Ubuntu`的硬盘存储管理
    1. 掌握`Ubuntu`的系统和服务管理
1. 实验内容与要求：
    1. 实验内容01：通过`CLI`创建用户账户、查看用户和组信息、添加至用户组、从用户组删除
    1. 实验内容02：创建和删除文件、查看文件属性、修改文件属性、查找文件、查找文件内容
    1. 实验内容03：新增硬盘、在线扩容已有文件系统
    1. 实验内容04：安装`mariadb-server`、管理`mysql`服务
1. 实验条件：
    1. `host os`宿主机环境：`Desktop`(`macOS`或`Windows`或`Linux`)或`Server`
    1. `virtual machine`虚拟机环境：`VirtualBox`或其他虚拟机
    1. `guest os`客户机环境：`Ubuntu 16.04`或以上版本，推荐`Ubuntu 20.04`

#### LA-E03：Run Command Script in Ubuntu脚本编程

1. 实验性质：设计性
1. 实验学时：8
1. 实验目的：
    1. 熟悉`shell`脚本的用途、编辑环境、运行环境
    1. 掌握`shell`的编程要素
    1. 掌握`shell`的设计方法和编程方法
1. 实验内容与要求：
    1. 实验内容01：显示当前日期时间、执行路径、用户账户及所在的目录位置
    1. 实验内容02：判断一个文件是不是字符设备文件，并给出相应的提示信息
    1. 实验内容03：从键盘输入两个字符串，比较两个字符串是否相等
    1. 实验内容04：分别用for、while与until语句求从整数1到100的和
    1. 实验内容05：实现每天将主目录下的所有目录和文件归档并压缩为文件mybackup.tar.gz
1. 实验条件：
    1. `host os`宿主机环境：`Desktop`(`macOS`或`Windows`或`Linux`)或`Server`
    1. `virtual machine`虚拟机环境：`VirtualBox`或其他虚拟机
    1. `guest os`客户机环境：`Ubuntu 16.04`或以上版本，推荐`Ubuntu 20.04`

### 本课程与其他课程的联系

1. 先修课程：《计算机组成原理》或《计算系统基础》、《操作系统》
1. 后续课程：《软件工程综合实践》、《Linux应用开发》等

### 推荐教材和教学参考书

张金石 主编, 钟小平 汪健 副主编. Ubuntu Linux操作系统(第2版 微课版) [M]. 北京: 中国工信出版集团, 北京: 人民邮电出版社. 2020-06.

## 实验课程教学大纲
