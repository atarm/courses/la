# Linux运维管理Operations in Linux

## Basic Information实验基本信息

1. 实验性质：综合性
1. 实验学时：6
1. 实验目的：
    1. 掌握`Ubuntu`的用户与组管理
    1. 掌握`Ubuntu`的文件与目录管理
    1. 掌握`Ubuntu`的硬盘存储管理
    1. 掌握`Ubuntu`的系统和服务管理
1. 实验内容与要求：
    1. 实验内容01：通过`CLI`创建用户账户、查看用户和组信息、添加至用户组、从用户组删除
    1. 实验内容02：创建和删除文件、查看文件属性、修改文件属性、查找文件、查找文件内容
    1. 实验内容03：新增硬盘、在线扩容已有文件系统
    1. 实验内容04：安装`mariadb-server`、管理`mysql`服务
1. 实验条件：
    1. `host os`宿主机环境：`Desktop`(`macOS`或`Windows`或`Linux`)或`Server`
    1. `virtual machine`虚拟机环境：`VirtualBox`或其他虚拟机
    1. `guest os`客户机环境：`Ubuntu 16.04`或以上版本，推荐`Ubuntu 20.04`

## Preconditions前置条件

1. 已安装`VirtualBox`或其他虚拟机
1. 已安装`Ubuntu 16.04`或以上版本，推荐`Ubuntu 20.04`
1. 具备`Internet`连接

## Contents and Steps内容与步骤

### 实验内容01：通过`CLI`创建用户账户、添加至用户组、查看用户和组信息、从用户组删除

1. 使用`adduser`创建一个用户名为 **"`{姓名拼音全拼}_{学号后两位}_e02`"（如`zhangsan_68_e02`）** 的用户账户
1. 通用`cat /etc/passwd`、`cat /etc/group`、`groups <username>`、`id <username>`查看创建的新用户及组的信息
1. 使用`usermod -aG sudo <username>`将创建的新用户添加到`sudo`用户组
1. 通用`cat /etc/passwd`、`cat /etc/group`、`groups <username>`、`id <username>`查看创建的新用户及组的信息
1. 使用`gpasswd -d <username> sudo`将创建的新用户从`sudo`用户组删除
1. 通用`cat /etc/passwd`、`cat /etc/group`、`groups <username>`、`id <username>`查看创建的新用户及组的信息

:exclamation: :exclamation: :exclamation: :exclamation: :exclamation: :exclamation:

1. 请注意`sudo`权限
1. `adduser`是一个`perl`脚本，在`useradd`的基础上提供交互式界面用于简化选项的输入

:exclamation: :exclamation: :exclamation: :exclamation: :exclamation: :exclamation:

### 实验内容02：创建和删除文件、查看文件属性、修改文件属性、查找文件、查找文件内容

1. 使用`mkdir`创建一个名为 **"`{姓名拼音全拼}_{学号后两位}_e02_c02`"（如`zhangsan_68_e02_c02`）** 的目录
1. 通过`cd <dirname>`进入新创建的目录
1. 通过`pwd`查看当前工作目录
1. 使用`touch`创建一个名为`README.md`的文件
1. 通过`ls -l`查看文件属性
1. 使用`chmod a+w <filename>`修改文件属性（添加所属组、其他用户的写权限）
1. 通过`ls -l`查看文件属性
1. 使用`echo "this is README file" >> <filename>`输入内容到文件
1. 通过`find / -name "*.md" 2> /dev/null`查找以`md`为文件名后缀的文件
1. 通过`locate -r \\.md$`查找以`md`为文件名后缀的文件
1. 通过`grep -i readme README.md`查找文件中包含`readme`（不区分大小写）的内容
1. 通过`grep -i r.*d README.md`查找文件中包含正则表达式`r.*d`（不区分大小写）的内容
1. 通过`cat README.md | grep -i r.*d`查找文件中包含正则表达式`r.*d`（不区分大小写）的内容
1. 通过`wc README.md`统计文件内容的行数、字数和字节数
1. 通过`cd ..`返回父目录
1. 通过`rm <dirname>`尝试删除目录
1. 通过`rmdir <dirname>`尝试删除目录
1. 使用`cp -r <dirname> <dirname>.bak`复制备份目录
1. 通过`rm -r <dirname>`删除目录及其子文件

❗❗❗❗❗❗

1. 请注意`sudo`权限
1. 使用`locate`前可能需要先运行`apt install locate`安装`locate`，并运行`sudo updatedb`更新`locate`的数据库

❗❗❗❗❗❗

### 实验内容03：新增硬盘、在线扩容已有文件系统

❗❗❗❗❗❗

请确保`ubuntu`安装时将根目录所在分区加入到`LVM`（即根目录所在分区为`LV`），否则，无法扩容根目录所在文件系统的容量

❗❗❗❗❗❗

1. 新增硬盘
    1. 通过`virtual box`创建并添加新硬盘（路径为`Settings>>>Storage>>>Controller:SATA>>>Adds Hard Disk>>>Create>>>Choose>>>OK`）
1. 创建分区
    1. 通过`lsblk`查看新创建并添加的硬盘名称（ ⚠️ **以下假定设备名称为`sdb`，请按实际情况修改设备名称** ⚠️ ）
    1. 使用`fdisk /dev/sdb`创建分区，依次输入以下命令
        1. `n`(add a new partition)
        1. `p`(primary)
        1. `<enter>`(default partition number 1)
        1. `<enter>`(default first sector)
        1. `<enter>`(default last sector)
        1. `t`(change a partition type)
        1. `8e`(Linux LVM)
        1. `w`(write table to disk and exit)
    1. 通过`lsblk`查看分区创建情况（显示`sdb1`分区表示已创建新分区）
1. 创建`pv`
    1. 使用`pvcreate /dev/sdb1`创建`pv`
    1. 通过`pvdisplay`查看`pv`创建情况（显示`--- NEW Physical volume ---`表示已创建新`PV`）
1. 扩容`vg`
    1. 通过`vgdisplay`查询`vg name`（ ⚠️ **以下假定`vg name`为`ubuntu-vg`，请按实际情况修改`vg name`** ⚠️ ）
    1. 使用`vgextend ubuntu-vg /dev/sdb1`将新的`pv`加入到`vg`
1. 扩容`lv`
    1. 通过`df -h`查询`/`目录所属`lv`（ ⚠️ **以下假定`/`目录所属`lv name`为`/dev/mapper/ubuntu--vg-ubuntu--lv`，请按实际情况修改`lv name`** ⚠️ ）
    1. 使用`lvextend -l +100%FREE /dev/mapper/ubuntu--vg-ubuntu--lv`将全部空闲空间分配给`/`目录所属`lv`
1. 扩容`fs`
    1. 使用`resize2fs /dev/mapper/ubuntu--vg-ubuntu--lv`扩容文件系统
    1. 通过`df -h`验证扩容`/`目录所在文件系统情况

:exclamation: :exclamation: :exclamation: :exclamation: :exclamation: :exclamation:

1. 请注意`sudo`权限
1. `ext`文件系统，使用`resize2fs`动态调整文件系统容量；`xfs`文件系统，使用`xfs_growfs`动态调整文件系统容量

:exclamation: :exclamation: :exclamation: :exclamation: :exclamation: :exclamation:

### 实验内容04：安装`mariadb-server`、管理`mysql`服务

1. 安装`mariadb-server`：`apt install mariadb-server`
1. 查看`mysql`服务状态：`service mysql status`或`systemctl status mysql.service`
1. 停止`mysql`服务：`service mysql stop`或`systemctl stop mysql.service`
1. 查看`mysql`服务是否开机启动：`systemctl is-enabled mysql.service`
1. 禁止`mysql`服务开机启动：`systemctl disable mysql.service`
1. 查看`mysql`服务是否开机启动：`systemctl is-enabled mysql.service`
1. 设置`mysql`服务开机启动：`systemctl enable mysql.service`
1. 查看`mysql`服务状态：`service mysql status`或`systemctl status mysql.service`
1. 查看`mysql`服务日志：`journalctl -u mysql.service`

## Bibliographies参考文献

1. [匿名. LVM添加硬盘并扩容至已有分区(DB/OL). 2020-07-31.](https://cloud.tencent.com/developer/article/1671893)
1. [阮一峰. Systemd 入门教程：命令篇(DB/OL). 2016-03-07.](https://www.ruanyifeng.com/blog/2016/03/systemd-tutorial-commands.html)
1. [阮一峰. Systemd 入门教程：实战篇(DB/OL). 2016-03-08.](https://www.ruanyifeng.com/blog/2016/03/systemd-tutorial-part-two.html)
1. [阮一峰. Systemd 定时器教程(DB/OL). 2018-03-30,](http://www.ruanyifeng.com/blog/2018/03/systemd-timer.html)
1. [阮一峰. Node 应用的 Systemd 启动(DB/OL). 2016-03-12.](http://www.ruanyifeng.com/blog/2016/03/node-systemd-tutorial.html)
1. [Linux存储管理：LVM](https://zhuanlan.zhihu.com/p/105553297)
1. [Linux 扩容 / 根分区(LVM+非LVM)](https://zhuanlan.zhihu.com/p/83340525)
