# Shell脚本编程Shell Script Programming

## Basic Information基本信息

1. 实验性质：设计型
1. 实验学时：8
1. 实验目的：
    1. 熟悉`shell`脚本的用途、编辑环境、运行环境
    1. 掌握`shell`的编程要素
    1. 掌握`shell`的设计方法和编程方法
1. 实验内容与要求：
    1. 实验内容01：显示当前日期时间、执行路径、当前登录用户名称及当前所在的目录位置
    1. 实验内容02：判断并输出一个文件的类型
    1. 实验内容03：从键盘输入两个字符串，比较并输出两个字符串的大小
    1. 实验内容04：分别用for、while与until语句求从整数1到100的和
    1. 实验内容05：实现每天将主目录下的所有目录和文件归档并压缩为文件mybackup.tar.gz
1. 实验条件：
    1. `host os`宿主机环境：`Desktop`(`macOS`或`Windows`或`Linux`)或`Server`
    1. `virtual machine`虚拟机环境：`VirtualBox`或其他虚拟机
    1. `guest os`客户机环境：`Ubuntu 16.04`或以上版本，推荐`Ubuntu 20.04`

## Preconditions前置条件

1. 已安装`VirtualBox`或其他虚拟机
1. 已安装`Ubuntu 16.04`或以上版本，推荐`Ubuntu 20.04`
1. 具备`Internet`连接

## Contents and Steps内容与步骤

### 实验内容01：显示当前日期时间、执行路径、当前登录用户名称及当前所在的目录位置

1. 脚本执行时输出`rc_in_ubuntu_<{姓名拼音全拼}_{学号后两位}>`，如`rc_in_ubuntu_zhangsan_68`
1. 请使用`` ` ``命令替换显示当前日期时间
1. 请使用变量内容替换显示执行路径
1. 请使用`$()`命令替换显示当前登录用户名称
1. 请使用`eval`命令替换显示当前所在的目录位置

### 实验内容02：判断并输出一个文件的类型

1. 脚本执行时输出`rc_in_ubuntu_<{姓名拼音全拼}_{学号后两位}>`，如`rc_in_ubuntu_zhangsan_68`
1. 判断并输出`/dev/sda`的类型
1. 如果文件不存在，则输出提示文件不存在

### 实验内容03：从键盘输入两个字符串，比较并输出两个字符串的大小

1. 脚本执行时输出`rc_in_ubuntu_<{姓名拼音全拼}_{学号后两位}>`，如`rc_in_ubuntu_zhangsan_68`
1. 字符串里允许出现空格

### 实验内容04：分别用for、while与until语句求从整数1到100的和

1. 脚本执行时输出`rc_in_ubuntu_<{姓名拼音全拼}_{学号后两位}>`，如`rc_in_ubuntu_zhangsan_68`

### 实验内容05：实现每天将`home`目录下的所有目录和文件归档并压缩为文件mybackup.tar.gz

1. 脚本执行时输出`rc_in_ubuntu_<{姓名拼音全拼}_{学号后两位}>`，如`rc_in_ubuntu_zhangsan_68`

## Bibliographies
