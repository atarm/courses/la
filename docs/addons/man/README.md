# Manual

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Synopsis Syntax](#synopsis-syntax)
2. [Common Options](#common-options)
3. [Beyond man](#beyond-man)
4. [Bibliographies](#bibliographies)

<!-- /code_chunk_output -->

## Synopsis Syntax

>they are loosely based on EBNF
>
>[Understand synopsis in manpage](https://newbedev.com/understand-synopsis-in-manpage)

>The following conventions apply to the SYNOPSIS section and can be used as a guide in other sections.
>
>+ **`bold text`**: type exactly as shown.
>+ _<u>`italic text`</u>_: replace with appropriate argument.
>+ `[-abc]`: any or all arguments within [ ] are optional.
>+ `-a|-b`: options delimited by | cannot be used together.
>+ <u>`argument`</u> `...`: argument is repeatable.
>+ `[`<u>`expression`</u>`] ...`: entire expression within [ ] is repeatable.
>
>Exact rendering may vary depending on the output device.  For instance, man will usually not be able to render italics when running in a terminal, and will typically use underlined or coloured text instead.
>
>The command or function illustration is a pattern that should match all possible invocations.  In some cases it is advisable to illustrate several exclusive invocations as is shown in the SYNOPSIS section of this manual page.
>
>`man man`

🔖 下划线容易与超链接混淆，可以使用其他形式代替（如，angle brackets尖括号`<argument>`）。

## Common Options

```bash {.line-numbers}
alias man='man -a'      # display all of the available manual pages contained within the manual.
```

## Beyond man

```bash {.line-numbers}
help <builtin>
```

## Bibliographies

1. `man man`
1. `man 7 man-pages`
1. [How to read man page SYNOPSIS](https://medium.com/@jaewei.j.w/how-to-read-man-page-synopsis-3408e7fd0e42)
1. [Understand synopsis in manpage](https://newbedev.com/understand-synopsis-in-manpage)
1. 🌟 [Beginners Guide to man Pages](http://www.tfug.org/helpdesk/general/man.html)
