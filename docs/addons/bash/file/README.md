# 文件处理

## 文件元数据

除了`ls`外，以下命令也可用于查询文件元数据

```bash {.line-numbers}
stat
wc
du
```

## 路径与文件名

```bash {.line-numbers}
realpath (1)         - print the resolved path
readlink (1)         - print resolved symbolic links or canonical file names
dirname (1)          - strip last component from file name
basename (1)         - strip directory and suffix from filenames
```
