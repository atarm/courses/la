# Expansions扩展

## 扩展的类型

>The types and order of expansions is:
>
>1. brace expansion大括号（`{}`）扩展
>1. tilde expansion波浪线（`~`）扩展
>1. parameter and variable expansion参数和变量扩展
>1. arithmetic expansion算术扩展
>1. command substitution (done in a left-to-right fashion)命令替换
>1. word splitting分词
>1. filename expansion文件名扩展
>
>>1. [Bash Reference Manual. 3.5 Shell Expansions.](https://www.gnu.org/software/bash/manual/html_node/Shell-Expansions.html)

1. `~`扩展
1. `?`字符扩展
1. `*`字符扩展
1. 方括号扩展
1. 大括号扩展
1. 变量扩展
1. 子命令扩展
1. 算术扩展

## Pattern Expansions模式扩展
