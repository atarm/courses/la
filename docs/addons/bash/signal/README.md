# Signal

## Keyboard Shortcuts

1. `<ctrl + c>`: kill foreground process
1. `<ctrl + d>`: send `EOF`
1. `<ctrl + z>`: suspend foreground process
1. `<ctrl + s>`: 
