# Linux Distributions

<!--
## `deb-based`/`apt-based`/`debian-based`

### `debian`

### `ubuntu`

### `elementaryos`

### `deepin`

### `kali linux`

### `Mint`

## `rpm-based`

### `suse`

### `opensuse`

## `redhat-based`

### `rhel(redhat enterprise linux)`

### `fedora`

## `slackware-based`

## `pacman-based`/`arch-based`

## `alpine`

-->

## Bibliographies

1. 🌟 ✨ [distrowatch](https://distrowatch.com/). <https://distrowatch.com/>
1. Wikipedia. List of Linux distributions[DB/OL]. <https://en.wikipedia.org/wiki/List_of_Linux_distributions>. 2021-08-25.
1. Wikipedia. Debian[DB/OL]. <https://en.wikipedia.org/wiki/Debian>. 2021-08-18.
1. Wikipedia. Alpine Linux[DB/OL]. <https://en.wikipedia.org/wiki/Alpine_Linux>. 2021-08-25.
1. Wikipedia. Arch Linux[DB/OL]. <https://en.wikipedia.org/wiki/Arch_Linux>. 2021-08-17.
1. Wikipedia. SUSE Linux[DB/OL]. <https://en.wikipedia.org/wiki/SUSE_Linux>. 2021-08-07.
1. Wikipedia. Slackware[DB/OL]. <https://en.wikipedia.org/wiki/Slackware>. 2021-08-19.
