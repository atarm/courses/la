# `Ubuntu Linux`

## 内容结构

```sh {.line-numbers}
.
├── ch01_meeting_ubuntu     6课时，第1章、第5章、第9.1节、第9.2节、第12.1.3节
├── ch02_user_mgt           2课时，第2章
├── ch03_file_mgt           2课时，第3章
├── ch04_storage_mgt        2课时，第4章、第12.1.5节
├── ch05_sys_mgt            2课时，第6章
└── ch06_shell_script       4课时，第8章
```
