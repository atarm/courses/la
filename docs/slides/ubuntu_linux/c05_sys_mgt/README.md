---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_System and Service Management_"
footer: "@aRoming"
math: katex
---


<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# System and Service Management系统及服务管理

---

## Contents

1. :star2: `process` management进程管理
1. :star2: `system` and `service` management系统和服务管理
1. `job` schedule作业调度
1. `log` management日志管理

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `process` Management进程管理

---

1. 从`admin`的角度，不关注`process`的内部机制，只关注`process`的控制管理
1. 从`admin`的角度，关注`PID`
1. `process`是`os`的资源分配单位
1. 一个`program`可以启动成多个`process`，一个`process`可以有多个`child process`

---

1. `program`是一个存储在磁盘某个目录中的可执行文件。内核使用`exec`函数，将`program`读入内存，并执行程序
1. `process`是`program`的执行实例，某些`OS`用`task`（任务）表示正在被执行的程序
1. `UNIX`系统确保每个在同一台机器上的`process`都有一个唯一的数字标识符（非负整数），称为`PID`
1. 通常，一个`process`只有一个控制线程`thread`，多个`thread`也可以充分利用多处理哭系统的并行能力
1. 与`process`相同，`thread`也有`tid`，但，`tid`只在它所属的`process`有效

>`UNIX`环境高级编程（第3版）.8-11.

---

## `process`的类型

1. `foreground process`前台进程/`interactive process`交互进程：`stdin attached`
1. `background process`后台进程：`stdin detached`
1. `daemon process`守护进程；一种特殊的`background process`
1. `service`服务：通常随`OS`的启动而启动成`daemon process`：`daemon process`全小写且一般加个`d`后缀

>[阮一峰. Linux 守护进程的启动方法(DB/OL). 2016-02-28,](https://www.ruanyifeng.com/blog/2016/02/linux-daemon.html)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
