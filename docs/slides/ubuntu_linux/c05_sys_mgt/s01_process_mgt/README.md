---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Process Management_"
footer: "@aRoming"
math: katex
---

<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Process Management进程管理

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [`Linux`的进程](#linux的进程)
2. [进程的`CRUD`](#进程的crud)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Linux`的进程

>1. [阮一峰. Linux 守护进程的启动方法.](https://www.ruanyifeng.com/blog/2016/02/linux-daemon.html)

---

1. `task` == `process`
1. `job` == `task`+
1. 进程的类型：
    1. 交互式进程
    1. 批处理进程<!--TODO:non-interactive process-->
    1. 守护进程
        1. 系统守护进程：提供本地服务
        1. 网络守护进程：提供网络服务

<!--TODO:
1. `init process`是所有`process`的`parent`

systemd进程呢？
-->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 进程的`CRUD`

---

### 进程的`Create`

1. 手动启动
1. 调度启动

---

### 进程的`Retrieve`

```bash {.line-numbers}
jobs                 # Display status of jobs.
ps (1)               - report a snapshot of the current processes.
top (1)              - display Linux processes
cat /proc/<process-id>/*
job
```

---

### 进程的`Update`

```bash {.line-numbers}
fg [job_spec]        # Move job to the foreground.
bg [job_spec ...]    # Move jobs to the background.
<ctrl-z>             # suspend foreground process

nice (1)             - run a program with modified scheduling priority
renice (1)           - alter priority of running processes
```

---

### 进程的`Delete`

```bash {.line-numbers}
kill (1)             - send a signal to a process
killall (1)          - kill processes by name
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
