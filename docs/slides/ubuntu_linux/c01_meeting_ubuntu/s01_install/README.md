---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Install The Linux_"
footer: "@aRoming"
math: katex
---

<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Install The Linux安装Linux

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [安装方式](#安装方式)
2. [Naming Schemes内核版本号规则](#naming-schemes内核版本号规则)
3. [查询内核版本号](#查询内核版本号)
4. [查询发行版版本号](#查询发行版版本号)

<!-- /code_chunk_output -->

---

## 安装方式

1. 物理机安装：
    1. `bare machine`：单系统，多重系统
    2. `bootable Ubuntu USB stick`
2. 虚拟机安装：
    1. `desktop virtual machine`
    2. `Win10 + WSL(Windows Subsystem for Linux)`
    3. `container/docker`：<https://labs.play-with-docker.com/>
    4. `cloud virtual machine`

---

1. [Ubuntu System Requirements(Desktop).](https://help.ubuntu.com/community/Installation/SystemRequirements)
2. [Ubuntu System Requirements(Server).](https://ubuntu.com/server/docs/installation)
3. [Windows Subsystem for Linux Installation Guide for Windows 10.](https://docs.microsoft.com/en-us/windows/wsl/install-win10)

---

![height:600](./.assets/image/docker_run_ubuntu.png)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Naming Schemes内核版本号规则

---

1. `<主版本>.<次版本>.<修订版本>-[附版本]`
1. `主版本`+`次版本`代表当前内核版本号
1. `附版本`由发行厂商定义，可省略

---

### Stable and Unstable Version稳定与开发版本

1. `odd-even` scheme: `2.x`，使用奇偶数区分
    1. `unstable`: 奇数代表实验版本
    1. `stable`: 偶数代表稳定版本
1. `mainline-stable-longterm` scheme: since `3.0`
    1. `mainline`/`unstable`/`development` or `pre-release`: `<major>.<minor>` or `<major>.<minor>-rc<number>`
    1. `stable` and `longterm`: `<major>.<minor>.<patch>`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 查询内核版本号

---

1. `uname -a`
2. `hostnamectl`
3. `cat /proc/version`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 查询发行版版本号

---

1. `uname -a`（有发行版名称，无版本号）
2. `hostnamectl`
3. `cat /proc/version`

---

1. `lsb_release -a`
2. `sudo dmesg | grep -i linux`
3. `cat /etc/*-release`: `cat /etc/lsb-release`, `cat /etc/os-release`
4. `cat /etc/issue`
5. use query in `package manager`
    1. `rpm-based`: `rpm -q nano`, `dnf info nano`
    2. `deb-based`: `dpkg -s nano`, `apt policy nano`

>[查看 Linux 发行版名称和版本号的 8 种方法[DB/OL].](https://linux.cn/article-9586-1.html)

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
