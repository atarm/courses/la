---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Package Manager and Distribution_"
footer: "@aRoming"
math: katex
---

<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Package Manager and Distribution包管理器与发行版

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [`AOSP` to `Android` VS `Linux` to `Linux Distro`](#aosp-to-android-vs-linux-to-linux-distro)
2. [Package Manager](#package-manager)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `AOSP` to `Android` VS `Linux` to `Linux Distro`

---

1. $\text{Android} \approx \text{AOSP} + \text{GMS}$
    - `AOSP(Android Open Source Project)`
    - `GMS(Google Mobile Services)`
1. $\text{Linux Distro} \approx \text{GNU/Linux(kernel)} + \text{Application Kits} + \text{Application Package Manager}$

---

![height:520](./.assets/image/android_stack_480.jpeg)

>[About the Android Open Source Project](https://source.android.com/)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Package Manager

---

### `Linux`软件安装方式

1. 离线安装
    1. 可执行文件复制安装：`cp`/`mv`
    2. 源码编译安装：`./configure && make && make install`
    3. 软件包（安装包）安装：将可执行文件、配置文档和帮助文档等打包成一个软件包文件，用户通过相应的`package manager`执行软件的安装、卸载、升级和查询等操作
2. 在线安装

---

### `yum`/`dnf`, `apt`,`pacman` and `snap`

1. `.rpm`：`RPM(RedHat Package Manager)`（离线安装），`YUM(Yellowdog Updater, Modified)`/`DNF(Dandified YUM)`（在线安装）
1. `.deb`：`DPKG(Debian PacKaGer)`（离线安装）, `APT(Advanced Package Tool)`（在线安装）
1. `.pacnew`：`PACMAN(PACkage MANager)`（在线安装）

>1. [yum (software)](https://en.wikipedia.org/wiki/Yum_(software))
>1. [APT (software)](https://en.wikipedia.org/wiki/APT_(software))
>1. [Arch Linux#Pacman](https://en.wikipedia.org/wiki/Arch_Linux#Pacman)

---

1. `.snap`：`snap`

>[Snap (package manager)](https://en.wikipedia.org/wiki/Snap_(package_manager))

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `deb-based`

---

#### `Debian`

>[Debian](https://en.wikipedia.org/wiki/Debian)

---

#### `Ubuntu`

---

1. `Ubuntu`由英国`Canonical`公司发布，他们提供商业支持。它是基于自由软件，其名称来自非洲南部祖鲁语或科萨语的"Ubuntu"一词（译为"乌班图"），意思是"人性"、"我的存在是因为大家的存在"，是非洲传统的一种价值观
1. `Canonical`由南非企业家`Mark Shuttleworth`创立
2. 2004-10-20发布首个版本`Ubuntu 4.10`，以`Debian`为开发蓝本，每半年发布新版本（4月、10月）
3. 2005-07-08成立`Ubuntu基金会`

>[Wikipedia: Ubuntu](https://zh.wikipedia.org/zh-cn/Ubuntu)

---

##### `Ubuntu`版本号规则

1. 根据发行的年月命名，如`Ubuntu 20.04`，`20`表示年份，`04`表示4月份
2. 发行代号由`形容词+动物名称`组成，并且这两个词的首字母相同，如`Focal Fossa`，从`Ubuntu 6.06 LTS`（唯一一次在6月份发布）开始，首字母按字母表顺序排列
3. `LTS(Long Term Support)`版本：每两年发布一次，偶数年份的4月发布的为`LTS`，自`Ubuntu 12.04 LTS`开始提供5年支持，非`LTS`提供1.5年支持

---

##### `Ubuntu Desktop` VS `Ubuntu Server`

1. `Ubuntu Desktop`
2. `Ubuntu Server`
3. `Ubuntu for IoT`
4. `Ubuntu Cloud`

---

1. _**The first difference is in the CD contents.**_ The "Server" CD avoids including what Ubuntu considers desktop packages (packages like X, Gnome or KDE), but does include server related packages (Apache2, Bind9 and so on). Using a Desktop CD with a minimal installation and installing, for example, apache2 from the network, one can obtain the exact same result that can be obtained by inserting the Server CD and installing apache2 from the CD-ROM.
2. _**The Ubuntu Server Edition installation process is slightly different from the Desktop Edition.**_ Since by default Ubuntu Server doesn't have a GUI, the process is menu driven, very similar to the Alternate CD installation process.

---

1. Before 12.04, Ubuntu server installs a server-optimized kernel by default. _**Since 12.04, there is no difference**_ in kernel between Ubuntu Desktop and Ubuntu Server since linux-image-server is merged into linux-image-generic.
2. For Ubuntu LTS releases before 12.04, the Ubuntu Desktop Edition only receives 3 years of support. This was increased to 5 years in Ubuntu LTS 12.04 In contrast, all Ubuntu LTS Server Edition releases are supported for 5 years.

>[What's the difference between desktop and server?](https://help.ubuntu.com/community/ServerFaq#What.27s_the_difference_between_desktop_and_server.3F)

---

##### `Ubuntu`包类型

1. `Main`：由`Canonical`公司支持的自由软件
2. `Restricted`：由`Canonical`公司支持的非自由软件，如，只能以二进制形式分发的显卡驱动程序
3. `Universe`：社区维护的自由软件
4. `Multiverse`：不是由`Canonical`公司支持的非自由软件，通常是由商业公司提供的软件

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `rpm-based`

---

![height:120](./.assets/image/260px-A_fedora_hat,_made_by_Borsalino.jpg)

1. Server
    1. `RHEL`(RedHat Enterprise Linux)：升级等服务收费
    2. `CentOS`(Community ENTerprise Operating System)：`RHEL`的裁剪重编译版
    3. `SUSE`：`SUSE Software Solutions Germany GmbH`公司
2. Desktop
    1. `Fedora`：社区版
    2. `SUSE desktop`

---

1. CentOS Linux 8, as a rebuild of RHEL 8, will end at the end of 2021. CentOS Stream continues after that date, serving as the upstream (development) branch of Red Hat Enterprise Linux.
2. `CentOS`创建者`Gregory M. Kurtzer`名为`Rocky Linux`的新项目

>[CentOS Project shifts focus to CentOS Stream](https://blog.centos.org/2020/12/future-is-centos-stream/)

---

![height:550](./.assets/image/centos-stream-relationship-to-rhel.png)

>[CentOS Stream: Everything You Need to Know About it](https://linuxiac.com/centos-stream/)

---

![width:1120](./.assets/image/fedora-centos-stream-rhel.png)

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `Arch Linux`

1. `pacman`

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `Alpine Linux`

---

`Alpine Linux` is a `Linux distribution` based on `musl` and `BusyBox`, designed for security, simplicity, and resource efficiency

>[Alpine Linux](https://en.wikipedia.org/wiki/Alpine_Linux)

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
