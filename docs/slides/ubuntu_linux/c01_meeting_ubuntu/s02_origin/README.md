---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_The Origin of Linux_"
footer: "@aRoming"
math: katex
---

<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Linux的由来The Origin of Linux

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [`UNIX` and `UNIX-like`](#unix-and-unix-like)
2. [`POSIX` and `SUS`](#posix-and-sus)
3. [`MINIX` and `GNU/Linux`](#minix-and-gnulinux)

<!-- /code_chunk_output -->

---

![height:450](./.assets/image/the_art_of_unix_programming.jpeg)
![height:450](./.assets/image/The.C.Programming.Language.Second.Edition.CN.jpg)
![height:450](./.assets/image/unix_a_history_and_a_memoir.jpeg)

---

![height:500](./.assets/image/osdi.1st.edition.jpg)
![height:500](./.assets/image/just_for_fun.jpeg)
![height:500](./.assets/image/the_cathedral_and_the_bazaar.jpeg)
<!-- ![height:300](./.assets/image/hackers_and_painters.jpeg) -->

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `UNIX` and `UNIX-like`

---

### `UNIX`和`C`之父Fathers of `UNIX` and `C`

>1. [Ken Thompson(KEN)](https://en.wikipedia.org/wiki/Ken_Thompson)
>1. [Dennis Ritchie(DMR)](https://en.wikipedia.org/wiki/Dennis_Ritchie)

---

#### DMR(right) and KEN(left)

![height:560](./.assets/image/Ken_Thompson_and_Dennis_Ritchie--1973.jpg)

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `Multics` and `UNIX`

---

1. `CTSS`(Compatible Time-Sharing System)：1964，`MIT`，`Model 33 teletypes`
1. `MULTICS`(Multiplexed Information and Computing Service, 多路信息与计算服务)：1965，`MIT`，`GE(General Electric)`，`BTL/Bell Labs`（1969-04退出）

>1. [Compatible Time-Sharing System](https://en.wikipedia.org/wiki/Compatible_Time-Sharing_System)
>1. [Multics](https://en.wikipedia.org/wiki/Multics)

---

1. `UNIX`：1969，`Kenneth Lane Thompson(KEN)`, `Dennis MacAlistair Ritchie(DMR)`, `Brian W. Kernighan(BWK)`, `Malcolm Douglas McIlroy(MDM)`(提出`pipe`的原始构想，`KEN`进行设计与实现), and `Joseph Frank Ossanna(JFO)`
    1. 最初缩写是`UNICS`(UNiplexed Information and Computing Service, 单路信息与计算服务)
    1. 1970年从`PDP-7`移植到`PDP-11`时，改名为`UNIX`
1. `Xenix`：1980，`UNIX` by `Microsoft`

---

#### `DEC PDP-7`（4K words）

![height:560](./.assets/image/pdp-7.jpg)

---

#### Standing DMR, Typing KEN, and Some PDP-11/20

![height:560](./.assets/image/standing-dmr_typing-ken_and_pdp-11.jpg)

---

#### `DEC PDP-11/20` Front Panel

![height:560](./.assets/image/original_pdp-11-20_front_panel.jpg)

---

#### `DEC PDP-11/70`

![height:560](./.assets/image/PDP-11-70.jpeg)

---

#### `DEC PDP-11/70` Front Panel

![height:540](./.assets/image/Pdp-11-70-front-panel.jpg)

---

#### `DEC PDP-11/70 DDS570` DEC Data System

![height:540](./.assets/image/PDP-11-70-DDS570%EF%BC%88disks_and_tape%EF%BC%89.jpg)

---

#### `DEC TU10` Tape Drive

![height:560](./.assets/image/DEC_TU10_tape_drive.jpg)

---

>Perhaps the most important achievement of Unix is to demonstrate that a powerful operating system or interactive use need not be expensive either in equipment or in human effort: it can run on hardware costing **_as little as $40,000 (equivalent to US$371,446 in 2022?)_**, and **_less than two man-years_** were spent on the main system software. We hope, however, that users find that the most important characteristics of the system are its simplicity, elegance, and ease of use.
>
>[D. M. Ritchie and K. Thompson. The UNIX Time-Sharing System](https://www.bell-labs.com/usr/dmr/www/cacm.html)

---

<!--TODO:
PDP-7的内存是多少？`Unics`使用多少？
-->

1. `DEC PDP-7`：1965年，`US$72,000` (equivalent to `US$619,109` in 2021, `US$668,604` in 2022), 18-bit小型机（字操作型），8K x 18-bit内存， `UNICS`4K × 18bit，用户程序4K × 18bit
2. `DEC PDP-11/20`：1970年，`US$11,800` (equivalent to `US$109,576.77` in 2022❓)，16-bit小型机（字节操作型），24KB内存，0.5MB磁盘，`UNIX`16KB，用户程序8KB

>1. UNIX 传奇：历史与回忆. 40-40.
>1. [陈硕. Kernighan《UNIX 传奇：历史与回忆》杂感.](https://www.cnblogs.com/Solstice/p/unix_bwk.html)

---

1. `IBM PC`：1981, 16-bit，16 kB - 640 kB内存
1. 1960s - 1970s，高端计算机的代表有`CDC 6600(60-bit)`、`IBM 7094/360/370`系列(36-bit或32-bit，128KB内存，约300万美元)、 `Clay-1`(64-bit)等，售价数百万美元起（折合2020年的上千万美元一台）

>1. [陈硕. Kernighan《UNIX 传奇：历史与回忆》杂感](https://www.cnblogs.com/Solstice/p/unix_bwk.html)
>1. [IBM Personal Computer.](https://en.wikipedia.org/wiki/IBM_Personal_Computer)

---

### 小型机Midrange Computer

1. 国外，小型机对应英文名是`minicomputer`和`midrange computer`，`minicomputer`一词由`DEC`公司于1965年创造
1. 国内，小型机习惯上用来指`UNIX服务器`，`UNIX服务器`具有区别`X86服务器`和大型主机的特有体系结构，基本上，各厂家`UNIX服务器`使用自家的`UNIX`版本的操作系统和专属的处理器

>[百度百科. 小型机.](https://baike.baidu.com/item/%E5%B0%8F%E5%9E%8B%E6%9C%BA/1472885)

---

1. `IBM`公司采用`Power`处理器和`AIX`操作系统
1. `Sun`、`Fujitsu`公司采用`SPARC`处理器和`Solaris`操作系统
1. `HP`采用`Itanium`处理器和`HP-UX`操作系统
1. `浪潮`采用`EPIC`处理器和`K-UX`操作系统
1. `Compaq`（已经被并入`HP`）采用`Alpha`处理器

>[百度百科. 小型机.](https://baike.baidu.com/item/%E5%B0%8F%E5%9E%8B%E6%9C%BA/1472885)

---

### `UNIX`的哲学

1. `kiss(Keep It Simple Stupid)`：程序应该只关注一个目标，并尽可能把它做好
1. `pipe`：让程序能够互相协同工作
1. `streaming`：应该让程序处理文本数据流，因为这是一个通用的接口

>Doug McIlroy, E. N. Pinson, B. A. Tague. Unix Time-Sharing System. The Bell System Technical Journal. 1978-07-08.

---

### `UNIX`的遗产

1. `C`
1. `modern OS`: `general-purpose OS`, `cross-platform OS`
1. `applications` and `tools`
1. `design principle` and `design pattern`
1. `hacker` and `OSS(Open Source Software)`

---

### `UNIX-like`

1. `UNIX-like`: behaves in a manner similar to a UNIX system, sometimes referred to as `UN*X` or `*nix`

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `POSIX` and `SUS`

---

1. `POSIX(Portable Operating Systems Interface)`
    - `IEEE` start at 1988, and now is `Austin Group`(`IEEE` + `ISO JTC 1 SC22` + `The Open Group`)
    - `POSIX`&trade; is a trademark of the `IEEE`
1. `SUS(Single Unix Specification)`
    - `Austin Group` start at 1994
    - certificated by `SUS` can use the `UNIX` trademark
    - `UNIX`&copy; is a registered trademark of `The Open Group`

---

>1. [POSIX](https://en.wikipedia.org/wiki/POSIX)
>1. [Single UNIX Specification](https://en.wikipedia.org/wiki/Single_UNIX_Specification)
>1. [The Open Group official register of UNIX Certified Products](https://www.opengroup.org/openbrand/register/)
>1. [The full register of certified products](https://www.opengroup.org/openbrand/register/index2.html)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `MINIX` and `GNU/Linux`

---

### `Minix`

1. `Minix` were created by `Andrew S. Tanenbaum` for educational purposes, `Minix` is now developed as open-source software
1. `Minix` was first released in 1987
1. `Linus Torvalds` used and appreciated `Minix`

>1. [Minix.](https://en.wikipedia.org/wiki/Minix)
>1. [Andrew S. Tanenbaum.](https://en.wikipedia.org/wiki/Andrew_S._Tanenbaum)
>1. [Andrew S. Tanenbaum's Home Page](https://www.cs.vu.nl/~ast/)

---

![height:600](./.assets/image/Andrew_S._Tanenbaum.jpg)

---

![height:600](./.assets/image/modern_operating_systems_4th_edition.jpeg)

---

### `GNU`

1. `UNIX`最初是开放源代码给厂家和科研机构，产生了许多衍生版本（两大流派：`SYS V`和`BSD`）
1. 1970年代末开始`UNIX`闭源，各个版本之间变得越来越不兼容
1. `GNU(GNU is Not Unix)`：1983/09，`Richard Stallman`，完全自由的`OS`及其他软件的集合，不是`UNIX`，但兼容`UNIX`
    - `FSF(Free Software Foundation)`
    - `GPL(General Public License)`

---

![height:560](./.assets/image/Richard_Matthew_Stallman_Cover_picture_for_O'Reilly_Media's_2002_book.jpeg)

Cover picture for O'Reilly Media's 2002 book Free as in Freedom: Richard Stallman's Crusade for Free Software

---

### `GNU/Linux`

1. `Linus Torvalds(born 1969)`：1991-09-17, `Linux 0.0.1`
    - `Linux` use `GPL` >>> `GNU/Linux`
    - `Linux`是一个`kernel`
    - 厂商在`Linux`的基础上叠加其他程序形成完整的`OS`称为`Linux Distribution(distro)`
1. `Linus's Unix`>>>`Linux`>>>`Linux Is Not Unix X`
1. `Linus`开发`Linux`的原因之二：`UNIX`版权纠纷、`Minix`的封闭（`Tanenbaum`不允许他人修改代码，可能是怕对`UNIX`产生侵权）

---

>1. [1992-01-29 The Tanenbaum-Torvalds Debate](https://www.oreilly.com/openbook/opensources/book/appa.html)
>1. [2006-05-10 Torvalds on the Microkernel Debate](https://slashdot.org/story/06/05/10/0439246/torvalds-on-the-microkernel-debate)
>1. [2006-05-12 Tanenbaum-Torvalds Debate Part II](https://www.cs.vu.nl/~ast/reliable-os/)

---

![height:560](./.assets/image/Linus_Torvalds_Talking_During_an_Interview_with_Linux_Magazine_2002.jpeg)

Linus Torvalds in 2022

---

### `UNIX` vs `Linux`

1. `UNIX`一般运行于小型机或工作站，1990s后，`x86`服务器与工作站的界限变得模糊
1. `Linux0.0.1`运行于`x86`

>1. UNIX 传奇：历史与回忆. 168-168.

---

1. `UNIX`使`hacker`文化达到极致，`Linux`事实上开创了`bazaar`风格的`open-source`开发模式、是`open-source`得以成为主流的重要原因
1. `Linux`没有使用`UNIX`的代码，但完全继承和发展了`UNIX`哲学，`UNIX`以`Linux`的形式获得重生
1. `Linux`不是`POSIX`、但事实上基于是`POSIX`兼容
    1. `Linux`是内核，不是一个完整的`OS`
    1. `POSIX`认证需要费用，`Huawei EulerOS`

>1. 大教堂与集市The Cathedral & The Bazaar.

---

### 服务器操作系统Top 500 Super Computers

1. 2018年起， `Top 500 Super Computers`全部运行`Linux`

>1. [OPERATING SYSTEM FAMILY / LINUX](https://www.top500.org/statistics/details/osfam/1/)
>1. [OPERATING SYSTEM FAMILY / WINDOWS](https://www.top500.org/statistics/details/osfam/2/)

---

![height:520](./.assets/image/Operating_systems_used_on_top_500_supercomputers.png)

>[Operating systems used on top 500 supercomputers](https://commons.wikimedia.org/wiki/File:Operating_systems_used_on_top_500_supercomputers.svg)

---

### 桌面操作系统份额Desktop `OS`

1. `Windows`: $\approx 75\%$
2. `MacOS`：$\approx 15\%$, `Darwin` based on `BSD 4.4`
3. `Chrome OS`: $\approx 2.2\%$
4. `Linux Desktop`: $\approx 2.5\%$
5. `unknown`

---

![height:520](./.assets/image/Desktop_Operating_System_Market_Share_Worldwide_202105-202205.png)

>[Desktop Operating System Market Share Worldwide(202105-202205).](https://gs.statcounter.com/os-market-share/desktop/worldwide)

---

![height:520](./.assets/image/StatCounter-os_combined-ww-monthly-202301-202401_desktop.png)

>[Desktop Operating System Market Share Worldwide(202301-202401).](https://gs.statcounter.com/os-market-share/desktop/worldwide)

<!--
<div id="desktop-os_combined-ww-monthly-202301-202401" width="600" height="400" style="width:600px; height: 400px;"></div>

<p>Source: <a href="https://gs.statcounter.com/os-market-share/desktop/worldwide">StatCounter Global Stats - OS Market Share</a></p><script type="text/javascript" src="https://www.statcounter.com/js/fusioncharts.js"></script><script type="text/javascript" src="https://gs.statcounter.com/chart.php?desktop-os_combined-ww-monthly-202301-202401&chartWidth=600"></script>
-->

---

### `UNIX`年表`UNIX` Timeline

---

![height:600](./.assets/image/Unix_timeline_s2.png)

---

![height:600](./.assets/image/Unix_timeline_s1.png)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

**:sparkles: :sparkles: :sparkles: Thank You :sparkles: :sparkles: :sparkles:**

**:ok: End of This Slide :ok:**
