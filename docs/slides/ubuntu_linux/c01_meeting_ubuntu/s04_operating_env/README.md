---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Operating Environment_"
footer: "@aRoming"
math: katex
---


<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Operating Environment使用环境

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [`CLI` and `GUI`](#cli-and-gui)
2. [`Shell`](#shell)
3. [Package Managers](#package-managers)
4. [Editors](#editors)
5. [Information and Help](#information-and-help)

<!-- /code_chunk_output -->

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `CLI` and `GUI`

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `CLI`

---

#### 物理`terminal`/`tty(teletypes/teletypewriter)`/`text input/output environment`

1. `CLI`也称为字符界面，$\text{CLI} \approx \text{terminal} + \text{shell}$
1. `terminal`：阅读和发送信息的设备/输入输出设备，穿孔卡和打印机，打字机和打印机
    1. `console`：一种特殊的`terminal`（输入输出环境与主机直接相联），早期为系统管理员专用的一种`terminal`，`Linux Distro`一般允许同时打开6个虚拟控制台（`Virtual Console`）和1个图形桌面（`Ubuntu 18.04`后不太一样）
1. `DEC VT100/VT102`，`DEC VT220`，`xterm`, `pterm`, `wyse60`, `ansi`, `ansi-bbs`

---

![height:600](./.assets/image/Punched_card_program_deck.agr.jpg)

---

![height:540](./.assets/image/real_teletypes_in_the_1940s.jpg)

>[The TTY demystified](http://www.linusakesson.net/programming/tty/index.php)

---

1. `Teletype Model 33`：只能输出大写字母，每秒10个字符
1. `Teletype Model 37`：能输出大小写字母，每秒15个字符
1. `UNIX`中许多命令都很短，原因之一是`Model 33`输入非常费力，而且输出速度太慢

>UNIX 传奇：历史与回忆. 55-55.

---

![height:600](./.assets/image/Teletype_Model_33.jpg)

---

![height:600](./.assets/image/Teletype_Model_37.jpg)

---

#### 仿真`terminal`/`pty(pseudo-tty)`

1. `pty`的组成
    1. `ptmx(pseudo-terminal master)`
    1. `pts(pseudo-terminal slave)`
1. 常用的`pty`
    1. `GUI`的`tty window`
    1. `text mode`
    1. `remote tty`

>[易生一世. 连接Linux服务器的终端仿真软件的termianl type详解](https://blog.csdn.net/taiyangdao/article/details/53166266)

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `GUI`

1. server: `X Window System`/`X11`/`X`
1. client:
    1. window manager: `Fvwm`, `IceWM`
    1. desktop environment: `GNOME`(`GTK+`), `KDE`(`QT`), `XFace`

>1. [Debian Wiki.WindowManager](https://wiki.debian.org/WindowManager)

<!--TODO: add it...

---

#### `tty`

1. `/dev/ttySn`/`/dev/tts/n`：串行端口终端

>[Linux中tty、pty、pts的概念区别(转)](https://www.jianshu.com/p/ad2a5e582c61)
-->

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Shell`

---

![height:600](./.assets/image/unix_system_architecture_01.png)

---

![height:600](./.assets/image/unix_system_architecture_02.jpg)

---

![height:560](./.assets/image/UI_Shell_and_GUI.jpg)

---

![width:1120](./.assets/image/pty_shell_in_a_telnet_session.png)

>[R. Koucha. Using pseudo-terminals (pty) to control interactive programs](http://www.rkoucha.fr/tech_corner/pty_pdip.html)

---

### `Shell`是什么

1. `shell`是用户级程序，不是`OS kernel`的一部分
1. `shell`是一个命令解释器，`shell`是对用户提出的运行程序的请求进行解释的程序
1. `shell`拥有内建的命令集
1. `shell script`将命令流程保存在文件中以供多次执行

>1. UNIX传奇：历史与回忆. 107-107.
>1. UNIX编程环境. 51-51.

---

### `shell`命令的基本语法

```bash {.line-numbers}
command [sub-command]* [options]* [arguments]*

utility_name [-a] [-b] [-c option_argument] \
                [-d|-e] [-f [option_argument]] [operand...]
```

>1. [Utility Argument Syntax.](https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap12.html)
>1. UNIX编程环境. 51-52.
>1. [Miryung Kim. Bash Command Line Interface.](http://web.cs.ucla.edu/~miryung/teaching/EE461L-Spring2012/labs/posix.html)

---

1. `shell`命令的各元素均区分大小写
1. `shell`的结束符有`\n`（交互式`CLI`即`<ENTER>`）、`;`、`&`，但通常只有`\n`后命令才开始执行

---

#### `shell`命令选项的语法

1. `-`后一般跟短选项（通常为单字符选项，且通常多个单字符选项可合并）
1. `--`后一般跟长选项（通常为短选项对应的全称选项）
1. 如果`--`后跟空白字符，则表示选项结束，空白字符后的字符作为命令参数而不是命令选项

>`--     Do not interpret any more arguments as options.`
>
>eg: `git checkout -- filename`
>
>`git checkout -- filename`

---

#### `shell`命令参数的语法

<!--TODO:
add it...
-->

---

### `Shell`的类型

1. `AT&T`
    1. `Bourne shell`/`sh`：控制流语法基于`ALGOL 68`
    1. 🌟 `GNU/bash(Bourne Again SH)`
    1. `dash(Debian Almquist Shell)`: `/bin/sh -> dash*`(`Ubuntu`)
    1. `ksh(Korn SHell)`
    1. 🌟 `zsh(Z SHell)`/`oh-my-zsh`
1. `BSD`
    1. `csh(C SHell)`

---

### 切换`Shell`

```sh {.line-numbers}
# show user default login shell
# attention, it may not be change even when you switch shell through <shell_name>
echo $SHELL

# show current shell
echo $0

# list available shells
cat /etc/shells

# temporary switch shell
<shell-name>

# change login shell, need to re-login or switch shell
chsh -s <shell_full_path> <username>
usermod -s <shell_full_path> <username>
```

---

### `bash`的基本用法

1. 文件结束符`EOF(End Of File)`：`<ctrl> + <d>`发送`EOF`
1. 命令历史：`history`, `!{cmd-history-id}`
    1. `~/.bash_history`
    1. `HISTSIZE`, `HISTFILESIZE`
1. 自动补全：`<tab>`
1. 别名：`alias`

---

1. 字符串：
    1. `单引号(')`：普通字符串
    1. `双引号(")`：`$`、`\`、`'`和`"`作为特殊字符并保留其特殊功能，其他都视为普通字符
    1. ``反引号(`)``：以字符串作为命令进行执行，执行结果作为最终结果

---

### `Command Substitution`命令替换

1. ``反引号(`)``
1. `$(...)`
    + 与``反引号(`)``的主要区别：`$(...)`易读、易嵌套
1. `eval`：二次扫描，如果第一遍扫描后，是个普通命令，则执行，如果仍含有变量的间接引用， 则对变量进行解引用

---

### 环境变量

+ 环境变量用于存储`shell`会话和工作环境的信息，命令、脚本或程序可通过环境变量获取系统信息、存取数据和配置信息
+ 环境变量一般采用`下划线风格`的全大写字母形式，以区别于其他类型的变量

---

```bash {.line-numbers}
echo $PATH
echo $HOME
echo $LOGNAME
echo $HOSTNAME
echo $PS1
echo $SHELL

# show all environment variables
env
printenv
```

---

```sh {.line-numbers}
echo $PS1   # 命令提示符
echo $PS2   # 命令续行提示符（续行输入反斜杠，续行结束输入<ctrl> + <d>）
echo $PS3   # 选择菜单提示符
echo $PS4   # <shell_name> -x <script_name>时的提示符
```

>[阮一峰. 命令提示符(DB/OL).](https://wangdoc.com/bash/prompt.html)

---

### `shell`的工作模式

1. `interactive shell`：与`terminal`交互，在命令提示符下输命令的`shell`
1. `non-interactive shell`
1. `login shell`：输入用户名和密码登陆的`shell`
1. `non-login shell`

>[Bash 启动环境](https://wangdoc.com/bash/startup.html)

---

1. `login` + `interactive`
    1. 虚拟控制台登录获取的第一个`shell`
    1. `ssh`登陆获取的`shell`
    1. `bash -l`切换的`shell`
    1. `su -l`切换用户的`shell`
1. `login` + `non-interactive`
    1. 通过`bash -l`运行的`script.sh`

---

1. `non-login` + `interactive`
    1. 通过`bash`切换的`shell`
    1. `GUI`下启动的`terminal`中的`shell`
    1. `su`切换用户的`shell`
1. `non-login` + `non-interactive`
    1. 运行`script.sh`

---

```bash {.line-numbers}
# =~ is regexp decision
if [[ $- =~ i ]]; then
    echo "interactive shell"
else
    echo "non-interactive shell"
fi
```

---

```bash {.line-numbers}
shell_type=$(echo $0)
if [[ $shell_type =~ "bash" ]]; then
    if [[ $(shopt login_shell) =~ on$ ]]; then
        echo "login bash shell"
    else
        echo "non-login bash shell"
    fi
elif [[ $shell_type =~ "zsh" ]]; then
    if [[ -o login ]]; then
        echo "zsh login shell"
    else
        echo "zsh non-login shell"
    fi
else
    echo "no bash or zsh"
fi
```

---

1. [韦易笑.千万别混淆 Bash/Zsh 的四种运行模式(DB/OL).](https://zhuanlan.zhihu.com/p/47819029)
1. [HailinGuo. bash 四种工作模式及其相关的配置文件(DB/OL).](http://hazirguo.github.io/articles/2017/bash_login_interactive_mode.html)

---

![height:540](./.assets/image/BashStartupFiles.png)

[Bash Initialisation Files](http://www.solipsys.co.uk/new/BashInitialisationFiles.html)

---

![height:600](./.assets/image/vim_home_profile.png)

---

![height:600](./.assets/image/vim_home_bashrc.png)

---

```sh {.line-numbers}
if [ -n "$BASH_ENV" ]; then 
    . "$BASH_ENV"
fi
```

---

![height:600](./.assets/image/login_and_non-login_rc.png)

---

1. `export`的`variable`和`function`能被`child shell`继承（`zsh`不能`export function`）
1. `alias`、`umask`、未被`export`的`variable`和未被`export`的`function`不能被`child shell`继承

---

1. `~/.bash_profile`：设置`environment variable`和`environment function`
1. `~/.bashrc`：设置`local variable`、`local function`、`alias`

❗ 由于经常存在`su`的情况，因此，一般只存在`~/.bashrc`（所有的设置均在该文件中），当存在`~/.bash_profile`时显式`source ~/.bashrc` ❗

>[bash启动脚本(DB/OL).](https://akaedu.github.io/book/ch31s04.html)

---

### 执行脚本的方式

1. `fork`方式：新建一个`child shell`执行
1. `source`方式：当前`shell`执行

---

### 导出环境变量

1. `export VAR=...`
1. 希望在当前`shell`中使用新添加在`~/.bashrc`里的环境变量，则运行`source ~/.bashrc`或`. ~/.bashrc`

---

### 输入输出重定向

+ 输入输出重定向：将`stdin`、`stdout`和`stderr`重定向到非默认位置
    1. `stdin`：文件描述符为`0`，`<`或`<<`
    1. `stdout`：文件描述符为`1`，`>`/`1>`或`>>`/`1>>`
    1. `stderr`：文件描述符为`2`，`2>`或`2>>`
+ `/dev/null`

---

```sh {.line-numbers}
cmd < file_or_other
cmd << marker   # here documents
>some data
>marker

cmd > file_or_other
cmd >> file_or_other

cmd >> /dev/null 2>&1
```

---

### `pipe`管道

+ `pipe`：将一个命令或程序的输出直接连接到另一个命令或程序的输入
+ `|`
+ `filter过滤器`：从输入流（一般是`stdin`）中接收输入并进行处理，然后将结果写入到输出流（一般指`stdout`）

```sh {.line-numbers}
who | wc -l
who | sort
who | grep "arm"
```

---

### `Regular Expression`正则表达式 -- 通配符

1. `.`：一个任意字符（用于"正则表达式"，不用于"命令扩展"）
1. `？`：一个任意字符（用于"命令扩展"，不用于"正则表达式"）
1. `*`：零个或多个任意字符串
1. `[]`：字符序列
1. `!`：`[]`中用`!`表示排除
1. `^`：只匹配行头
1. `$`：只匹配行尾

---

### `Regular Expression`正则表达式 -- 模式表达式

1. `?(模式列表)`：0或1个模式
1. `*(模式列表)`：0或多个模式
1. `+(模式列表)`：1或多个模式列表
1. `@(模式列表)`；1个模式
1. `!(模式列表)`：除了一个模式以外的任何模式

>1. [GAUNTHAN. Shell脚本中的通配符扩展](http://blog.leanote.com/post/gaunthan/通配符扩展)
>1. [阮一峰. Bash 的模式扩展](https://wangdoc.com/bash/expansion.html)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Package Managers

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `dpkg`

---

#### `dpkg`选项--查询类

```sh {.line-numbers}
-s|--status [<pkg-name>...]       Display package status details.
-p|--print-avail [<pkg-name>...]  Display available version details.
-L|--listfiles <pkg-name>...      List files owned by package(s).
-l|--list [<pattern>...]         List packages concisely.
-S|--search <pattern>...         Find package(s) owning file(s).
```

---

#### `dpkg`选项--安装卸载类

```sh {.line-numbers}
-i|--install <.deb filename>...
-r|--remove <pkg-name>...
-P|--purge <pkg-name>...
```

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `apt`

---

1. 查询软件包
1. 下载软件包或源码
1. 安装软件包：自动解决依赖关系
1. 删除软件包：提示依赖关系

---

#### `apt`重要文件

```sh {.line-numbers}
/etc/apt/sources.list           软件包源配置文件
/etc/apt/sources.list.d/        软件包源子配置文件
/etc/apt/apt.conf               apt配置文件
/etc/apt/apt.conf.d/            apt子配置文件
/etc/apt/preferences            版本参数
/var/cache/apt/archives/        存储已经下载的软件包
/var/cache/apt/archives/partial 存储正在下载的软件包
/var/lib/apt/lists/             存储已经下载的软件包详细信息
/var/lib/apt/lists/partial/     存储正在下载的软件包详细信息
/var/lib/dpkg/status            安装及卸载的软件包信息
```

---

#### `apt` --- Read

```sh {.line-numbers}
list - list packages based on package names
search - search in package descriptions
show - show package details

# sub-commands cannot find in man
depends
rdepends
policy
```

---

#### `apt`子命令 --- Create

```sh {.line-numbers}
install - install packages
reinstall - reinstall packages
```

---

#### `apt`子命令 --- Update

```sh {.line-numbers}
update - update list of available packages
upgrade - upgrade the system by installing/upgrading packages
full-upgrade - upgrade the system by removing/installing/upgrading packages
```

❗ `apt full-upgrade`对应`apt-get dist-upgrade` ❗

---

#### `apt`子命令 --- Delete

```sh {.line-numbers}
remove - remove packages
autoremove - remove automatically all unused packages
```

---

#### `update` VS `upgrade` VS `full-upgrade`

+ `update`: update package metadata
+ `upgrade` and `full-upgrade`: update package

:warning:

1. 建议`install/reinstall/upgrade/full-upgrade`之前先执行`update`，`sudo apt update && apt upgrade [pkg-name]`
1. `yum`中没有`update`和`upgrade`之分，在`update package`之前自动`update package metadata`

:warning:

---

+ `upgrade`: install available upgrades of all packages currently installed on the system from the sources configured via `sources.list`. New packages will be installed if required to satisfy dependencies, **_but existing packages will never be removed_**. If an upgrade for a package **_requires the removal of an installed package__** the upgrade for this package **_isn't performed_**.
+ `full-upgrade`: performs the function of upgrade but **_will remove currently installed packages_** if this is needed to upgrade the system as a whole.

---

+ 假设软件包`a`原先依赖`b`、`c`、`d`，现在`a`依赖`b`、`c`、`e`。`full-upgrade`会删除`d`安装`e`，并把`a`软件包升级，而`upgrade`会认为依赖关系改变而不执行升级`a`

>[Ubuntu里面update,upgrade和dist-upgrade的区别](http://www.guozet.me/post/Ubuntu-Update-Upgrade/)

---

+ One corner case where this might do you more harm than good is when you may have some custom software on your Linux system which may be dependent on the older packages, and these custom software which you have built and installed yourselves might not be known to the apt tool, so removing that package might not be the best thing to do!

>["apt upgrade vs full-upgrade" Differences Explained For Beginners!](https://embeddedinventor.com/apt-upgrade-vs-full-upgrade-differences-explained-for-beginners/)

---

#### `apt`子命令 --- 配置及维护类

```sh {.line-numbers}
edit-sources - edit the source information file

# sub-commands cannot find in man
clean
autoclean
```

---

### `aptitude`

Aptitude is front-end to advanced packaging tool which adds a user interface to the functionality, thus allowing a user to interactively search for a package and install or remove it. Initially created for Debain, Aptitude extends its functionality to RPM based distributions as well.

>[What is APT and Aptitude? and What's real Difference Between Them?](https://www.tecmint.com/difference-between-apt-and-aptitude/)

---

### `apt` VS `apt-get` VS `aptitude`

1. `apt`、`apt-get`、`aptitude`都是`APT`的`front-end`
1. `apt`和`apt-get`是`CLI`命令，`aptitude`是`CLI`字符图形界面工具（也提供`CLI`命令）

---

### `apt` VS `apt-get`

1. `16.04`是首个引入`apt`命令的稳定版本
1. `apt`是`apt-get`、`apt-cache`、`apt-config`三个命令的整合和改进（整合了大部分命令，小部分命令未纳入`apt`）<!--TODO:具体哪些命令没有纳入？-->
1. `apt`新增了进度条、可升级软件包提示等人机交互功能
1. `apt`新增了`apt list`、`apt edit-sources`子命令<!--TODO:还有哪些新增命令？-->

---

#### `apt-get`, `apt-cache`, `apt-config`

1. `apt-get`：安装软件包
1. `apt-cache`：查询软件包信息
1. `apt-config`：配置`APT`

---

#### `/etc/apt/sources.list`

1. 第1部分：软件源类型
    + `deb`
    + `deb-src`
1. 第2部分：URL
1. 第3部分：软件源对应的发行版本及软件源类型
    + `代号`：默认版本
    + `代号-security`：安全更新
    + `代号-updates`：推荐的一般更新
    + `代号-backports`：无支持的更新（通常还存在一些bug）
    + `代号-proposed`：预览版更新（相当于`updates`的测试版）

---

1. 剩余部分：软件源的标签（可存在多个标签）
    + `main`：`Canonical`维护的开源软件包
    + `universe`：`community`维护的开源软件包
    + `restricted`：设备生产商专有的设备驱动软件包
    + `multiverse`：有版权或受法律保护的软件包

---

#### `PPA(Personal Package Archives)`源

1. `Canonical`架设的，供软件开发人员创建自己的存储库，以快速发布非稳定版或个人软件包
1. <https://launchpad.net/ubuntu/+ppas>
1. `PPA`的配置以单独的文件形式存放在`/etc/apt/sources.list.d/`目录

```sh {.line-numbers}
ppa:<user>/<ppa-name>

# add ppa
add-apt-repository ppa:<username>/<ppa-name>
# remove ppa
add-apt-repository -r ppa:<username>/<ppa-name>
```

---

>[Using PPA in Ubuntu Linux [Complete Guide]](https://itsfoss.com/ppa-guide/)

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `snap`

1. 最初由`Canonical`公司为了`Ubuntu Touch`设计和构建
1. 基于`sandbox`/`container`模式
1. `Ubuntu 16.04`引入，一般安装在`/snap/`下

>[Snappy (包管理器)](https://zh.wikipedia.org/wiki/Snappy_(%E5%8C%85%E7%AE%A1%E7%90%86%E5%99%A8))

---

#### `snap`的优缺点

+ 优点：
    1. `sandbox`/`container`模式，运行环境与软件打包在一起，跨平台运行，进一步安全隔离
    1. 支持事务型自动更新
+ 缺点
    1. 软件包体积较大（连同运行环境一起打包）
    1. 目前软件数量较少

---

#### `snap`子命令--查询类

```sh {.line-numbers}
find             Find packages to install
info             Show detailed information about snaps
list             List installed snaps
```

---

#### `snap`常用子命令--安装卸载类

```sh {.line-numbers}
install          Install snaps on the system
remove           Remove snaps from the system
refresh          Refresh snaps in the system
revert           Reverts the given snap to the previous state
```

---

#### `snap`常用子命令--运行类

```sh {.line-numbers}
start            Start services
stop             Stop services
restart          Restart services
```

:point_right: `/snap/bin/`目录通常被添加到`$PATH`中，因此，也可以直接运行 :point_left:

---

#### `snap`常用子命令--配置维护类

```sh {.line-numbers}
disable          Disable a snap in the system
enable           Enable a snap in the system
switch           Switches snap to a different channel
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Editors

---

1. `ed`: a line-oriented text editor
1. `vi`：`Bill Joy`(`William Nelson Joy`)
    + `GNU/vim`(Vi IMproved)
1. `emacs`(Editor MACroS)
    + `GNU/Emacs`
1. `GNU/nano`(Nano's ANOther editor)

---

>1. [vi](https://en.wikipedia.org/wiki/Vi)
>1. [Vim (text editor)](https://en.wikipedia.org/wiki/Vim_(text_editor))
>1. [Emacs](https://en.wikipedia.org/wiki/Emacs)
>1. [GNU Emacs](https://en.wikipedia.org/wiki/GNU_Emacs)
>1. [GNU nano](https://en.wikipedia.org/wiki/GNU_nano)

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### `vi`/`vim`

---

#### `vi` father --- `Bill Joy`

![height:580](./.assets/image/Bill_Joy_at_World_Economic_Forum_(Davos),_2003-01_(cropped).jpg)

---

![height:540](./.assets/image/billjoy-650x488.jpg)

>[Bill Joy: Co-founder of Sun Microsystems](https://engineering.berkeley.edu/bill-joy-co-founder-of-sun-microsystems/)

---

1. `vim`提倡快速使用键盘（不包含方向键和数字键）
1. `vim`的4个模式
    1. `normal mode`普通模式
    1. `insert mode`插入模式
    1. `command-line mode`命令行模式/`command mode`命令模式
    1. `visual mode`可视模式

---

![width:1120](./.assets/image/vim-modes.png)

>[Vim](https://cs61a.org/articles/vim/)

---

![height:600](./.assets/image/Vim-(logiciel)-console.png)

---

#### `vim`常用命令

>[Vim Mode](https://www.pythonclassroom.com/python-cloud-options/cloud9/vim-mode)

---

![height:600](./.assets/image/vi-vim-cheat-sheet.gif)

---

##### `normal mode` -- Movement

```sh {.line-numbers}
h           Moves the cursor one character to the left
l           Moves the cursor one character to the right
k           Moves the cursor up one line
j           Moves the cursor down one line
nG or :n    Cursor goes to the specified (n) line
^F          Forward screenful
^B          Backward screenful
^f          One page forward
^b          One page backward
^U          Up half screenful
^D          Down half screenful
$           Move cursor to the end of current line
0           Move cursor to the beginning of current line
w           Forward one word
b           Backward one word
```

---

##### `normal mode` -- Text Manipulation

```sh {.line-numbers}
x           Delete character
dw          Delete word from cursor on
db          Delete word backward
dd          Delete line
d$          Delete to end of line
d^          Delete to beginning of line
yy          yank current line
y$          yank to end of current line from cursor
yw          yank from cursor to end of current word
p           paste below cursor
P           paste above cursor
u           Undo last change
U           Restore line
.           Repeat last command
```

---

##### `normal mode` to `insert mode`

```sh {.line-numbers}
a   Append text following current cursor position
A   Append text to the end of current line
i   Insert text before the current cursor position
I   Insert text at the beginning of the cursor line
o   Open up a new line following the current line and add text there
O   Open up a new line in front of the current line and add text there
```

---

##### `command-line mode`

```sh {.line-numbers}
:wq             Write file to disk and quit the editor
:q!             Quit (no warning)
:q              Quit (a warning is printed if a modified file has not been saved)
ZZ              Save workspace and quit the editor (same as :wq)
:w              Write workspace to original file
:w file         Write workspace to named file
:e file         Start editing a new file
:r file         Read contents of a file to the workspace
:/              pattern Search forward for the pattern
:?              pattern Search backward for the pattern
:g/p1/s//p2/g   replace every occurrence of p1 with p2
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Information and Help

---

### Information

```bash {.line-numbers}
which       # locate a command
whereis     # locate the binary, source, and manual page files for a command
whatis      # display one-line manual page descriptions
type        # display information about command type.

file        # determine file type
```

---

### Help

```bash {.line-numbers}
man man

info info               # only when the command has info page

# bash
help help
help <builtin_cmd>      # builtin command has no man page and info page
<cmd> --help

# zsh
run-help <builtin_cmd>|<external_cmd>
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗**End of This Slide**🆗
