---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Programming Environment_"
footer: "@aRoming"
math: katex
---


<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Programming Environment

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [编译器`GCC`与调试器`GDB`](#编译器gcc与调试器gdb)
2. [自动化构建工具`make`](#自动化构建工具make)
3. [自动化构建工具集`GNU Autotools`/`GNU Build System`](#自动化构建工具集gnu-autotoolsgnu-build-system)
4. [`GUI frameworks`: `GTK` and `Qt`](#gui-frameworks-gtk-and-qt)

<!-- /code_chunk_output -->

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 编译器`GCC`与调试器`GDB`

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 编译器`GCC`

---

1. `GCC(GNU Compiler Collection, GNU编译器套件)`：原全称为`GNU C Compiler`，后扩展到其他语言后改称为`GNU Compiler Collection`
1. `GCC`支持绝大多数`UNIX-like`和`Windows`

---

![width:1120](./.assets/image/c_compile_processes.png)

---

1. preprocessing: `cpp`
1. compile: `cll`
1. assemble: `as`
1. linking: `ld`
    1. static linking: static library, `libxxx.a`(archive file)
    1. dynamic linking: dynamic library, `libxxx.so`(shared object file)

---

<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 调试器`GDB`

---

1. `GDB(GNU Debugger)`
1. 为了进行程序调试，需在可执行文件中包含调试信息（变量类型、地址映射、源代码行号等）
1. 程序发布时应关闭编译器的`DEBUG`选项

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 自动化构建工具`make`

---

### `make`

1. `makefile`：使用一种规格语言（声明式语言）来描述程序的各个部分如何相互依赖
1. `make`：读取`makefile`并根据`makefile`的规则调用`compiler`等相关工具，基于`pattern-action`范式
1. `make`可只针对自上次编译后修改的部分进行编译
1. 代码变成可执行文件，叫`compile`；先编译这个，还是先编译那个（即编译的安排），叫做`build`

>1. UNIX 传奇：历史与回忆[M]. 117-117.
>1. [阮一峰. Make 命令教程[DB/OL]. 2015-02-20.](https://www.ruanyifeng.com/blog/2015/02/make.html)
>1. [阮一峰. 编译器的工作过程[DB/OL]. 2014-11-11.](http://www.ruanyifeng.com/blog/2014/11/compiler.html)

---

```sh {.line-numbers}
make [option]... [target]...
```

---

### `makefile`

```sh {.line-numbers}
<target> : <prerequisites> 
<tab><commands>
```

1. `commands`前，必须由一个`<tab>`（不能用多个`<space>`代替）
1. `<target>`必须
1. `<prerequisites>`和`<commands>`不是必须，但两者必须至少存在一个

```sh {.line-numbers}
clean:
    rm *.o
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 自动化构建工具集`GNU Autotools`/`GNU Build System`

---

![height:600](./.assets/image/autotools_dev_view_and_user_view.jpg)

---

1. `Autotools`的最终目标是收集系统配置信息以生成 **_适配运行环境_** 的`makefile`
    1. `autoconf`:  generates a `configure` script based on the contents of a `configure.ac` file
    1. `automake`: `automake` helps to create portable `Makefile`s
    1. `libtool`: helps manage the creation of static and dynamic libraries on various `UNIX-like` operating systems

>[GNU Autotools](https://en.wikipedia.org/wiki/GNU_Autotools)

---

![height:640](./.assets/image/Autoconf-automake-process.png)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `GUI frameworks`: `GTK` and `Qt`

---

1. `GTK`起源自`GIMP(GNU Image Manipulation Program)`，意为`GIMP Toolkit`，后加入`OO`和可扩展性，改称为`GTK+`，`GTK 4`后将`+`去掉
1. `GTK`本身使用`C`语言开发，但已支持绝大部分主流语言调用，也移植到其他主流`OS`（`Linux`、`macOS`、`Windows`）

---

1. `Qt`不仅拥有完善的`C++ GUI Library`，还集成了`DB`、`OpenGL`、多媒体库、网络、脚本库、`XML`、`WebKit`、`IPC`、`multi-thread`等
1. `Qt`支持主流`OS`，支持主流桌面`OS`、嵌入式和移动端（`Linux`、`macOS`、`iOS`、`Android`、`Windows`）

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

**:sparkles: :sparkles: :sparkles: Thank You :sparkles: :sparkles: :sparkles:**

**:ok: End of This Slide :ok:**
