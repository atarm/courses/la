---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Flow Control_"
footer: "@aRoming"
math: katex
---

<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Flow Control

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Lists of Commands](#lists-of-commands)
2. [Grouping Commands](#grouping-commands)
3. [Conditional Constructs](#conditional-constructs)
4. [Looping Constructs](#looping-constructs)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Lists of Commands

---

```sh {.line-numbers}
<command_01> && <command_02>

<command_01> || <command_02>
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Grouping Commands

---

```sh {.line-numbers}
(...)

{...}
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Conditional Constructs

---

### `if`

```sh {.line-numbers}
if test-commands; then
  consequent-commands;
[ elif more-test-commands; then
  more-consequents; ]
[ else alternate-consequents; ]
fi
```

>The **_test-commands_** list is executed, and if its **return status is zero**, the **_consequent-commands_** list is executed.
>>[Bash Reference Manual. 3.2.5.2 Conditional Constructs.](https://www.gnu.org/software/bash/manual/html_node/Conditional-Constructs.html)

---

### `case`

```sh {.line-numbers}
case word in
    [ [(] pattern [| pattern]…) command-list ;;]…
esac
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Looping Constructs

---

### `until`

```sh {.line-numbers}
until test-commands; do
    consequent-commands;
done
```

---

### `while`

```sh {.line-numbers}
while test-commands; do
    consequent-commands;
done
```

---

### `for-in`

```sh {.line-numbers}
for name [ [in [words …] ] ; ] do
    commands;
done
```

---

### `c-style-for`

```sh {.line-numbers}
for (( expr1 ; expr2 ; expr3 )) ; do
    commands ;
done
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗**End of This Slide**🆗
