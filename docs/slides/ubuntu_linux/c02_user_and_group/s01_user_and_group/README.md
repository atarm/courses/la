---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_User and Group用户与用户组_"
footer: "@aRoming"
math: katex
---

<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# User and Group

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [`user`的类型](#user的类型)
2. [`group`的类型](#group的类型)
3. [`uid` and `gid`](#uid-and-gid)
4. [Files in `user` and `group`](#files-in-user-and-group)
5. [`su` and `sudo`](#su-and-sudo)

<!-- /code_chunk_output -->

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `user`的类型

---

1. `root`/`superuser`
1. `regular user`
    1. `sudoer`
    1. other `regular user`
1. `system user`：系统或应用程序使用的专门的用户账户

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `group`的类型

1. `root group`/`superuser group`
1. `regular group`
    1. `sudo`
    1. other `regular group`
1. `system group`

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `uid` and `gid`

---

1. `uid`是一个数值，标识不同的用户，用户不能更改其`uid`
    1. `root`（或称为`superuser`）：`uid`为0
    1. `system user`：`uid`为1-499和65534
    1. 其他：`uid`大多数发行版从500开始（`ubuntu`从1000开始）
1. `gid`是一个数值，标识不同的用户组
    1. `root group`：`gid`为0
    1. `system group`：`gid`为1-499
    1. 其他：`gid`大多数发行版从500开始（`ubuntu`从1000开始）
1. `uid`隶属于一个`primary group`/`initial group`和多个附属组

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Files in `user` and `group`

---

1. `/etc/passwd`：用户账户及其相关信息（密码除外），`-rw-r--r-- root root`
1. `/etc/shadow`：用户密码（`hashed`），`-rw-r----- root shadow`
1. `/etc/group`：用户组相关信息（组密码除外），`-rw-r--r-- root root`
1. `/etc/gshadow`：组密码、组管理员等，`-rw-r----- root shadow`
1. `/etc/sudoers`：`sudoer`权限等信息，`-r--r----- root root`

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `su` and `sudo`

---

### `su`

1. `su -`/`su -l`：`login shell`，`profile`下的设置为切换后的用户的`profile`
1. `su`：`non-login shell`，`profile`下的设置为切换前的用户的`profile`

---

### `su` vs `sudo`

1. `su`：提供`target user`的密码
1. `sudo`：提供`current user`的密码
    1. `sudo -i, --login`
    1. `sudo su`

:point_right: `sudo`相当于受限的`su`（通过`/etc/sudoers`配置进行限制），且不需要知道`target user`的密码 :point_left:

---

### `visudo`

1. 互斥修改`/etc/sudoers`
1. 检查`/etc/sudoers`的语法

---

```sh {.line-numbers}
<${user}>       <${allowed_host}>=([${allowed_target_group}]:<${allowed_target_user}>)\
        [NOPASSWD]:<${allowed_commands}> | !<${denied_commands}>

%<${group}>     <${allowed_host}>=([${allowed_target_group}]:<${allowed_target_user}>)\
        [NOPASSWD]:<${allowed_commands}> | !<${denied_commands}>
```

---

```sh {.line-numbers}
# /etc/sudoers
...
a_user      ALL=(ALL)   !/usr/bin/passwd [A-Za-z0-9]+, !/usr/bin/passwd, !/usr/bin/passwd root
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

**:sparkles: :sparkles: :sparkles: Thank You :sparkles: :sparkles: :sparkles:**

**:ok: End of This Slide :ok:**
