---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_File Management_"
footer: "@aRoming"
math: katex
---

<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# File Management文件管理

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [目录管理](#目录管理)
2. [文件管理](#文件管理)
3. [文件权限](#文件权限)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 目录管理

---

### 目录管理---Create

```sh {.line-numbers}
mkdir [<OPTION>]... <DIRECTORY>...
```

---

### 目录管理---Read

```sh {.line-numbers}
cd [-L|[-P [-e]] [-@]] [dir]

cd
cd -
cd ~

pwd

ls [<OPTION>]... [<FILE>]...
```

---

### 目录管理---Update

```sh {.line-numbers}
mv [<OPTION>]... <SOURCE>... <DIRECTORY>
```

---

### 目录管理---Delete

```sh {.line-numbers}
# rmdir - remove empty directories
rmdir [<OPTION>]... <DIRECTORY>...

rm -r
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 文件管理

---

### 文件管理---Create

```sh {.line-numbers}
# touch - change file timestamps
touch [<OPTION>]... <FILE>...

# vim - Vi IMproved, a programmer's text editor
vim [<options>] [<file> ..]

# cp - copy files and directories
cp [<OPTION>]... <SOURCE>... <DIRECTORY>

# ln - make links between files
ln [<OPTION>]... <TARGET>... <DIRECTOR>
```

---

```sh {.line-numbers}
gzip

unzip

# tar - an archiving utility
tar 
# z: zip, c: create, x: extract, v: verbose, f: file
tar -zcvf
tar -zxvf
```

---

### 文件管理---Retrieve

---

#### Read

```sh {.line-numbers}
cat
more
less
head
tail
od
```

---

#### Statistic

```sh {.line-numbers}
wc
```

---

#### Find Content in File

```sh {.line-numbers}
grep
```

---

#### Find File

```sh {.line-numbers}
find
locate
```

---

#### Find Difference in File

```sh {.line-numbers}
cmp         # compare two files byte by byte
comm        # compare two sorted files line by line
diff        # compare files line by line
```

1. `cmp`主要用于判断两个文件是否内容相同，它工作快，适用于任何类型的文件，而不仅仅是文本文件
1. `diff`主要用于已知两个文件可能有所差别，进而需要准确地知道哪些行有什么不同的情况
1. `comm`和`diff`仅适用于文本文件

---

### 文件管理---Update

```sh {.line-numbers}
sort
mv
```

---

### 文件管理---Delete

```sh {.line-numbers}
rm
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 文件权限

---

### 文件访问者身份

1. `u(owner)`
1. `g(group)`
1. `o(others)`

---

### 文件访问权限

1. `r(read)`: 4
1. `w(write)`: 2
1. `x(execute)`: 1

---

### 文件访问者身份---Update

```sh {.line-numbers}
# chown - change file owner and group
chown [<OPTION>]... [<OWNER>][:[<GROUP>]] <FILE>...

# chgrp - change group ownership
chgrp [<OPTION>]... <GROUP> <FILE>...
```

---

### 文件访问权限---Update

```sh {.line-numbers}
# chmod - change file mode bits
chmod [<OPTION>]... <MODE>[,<MODE>]... <FILE>...
chmod [<OPTION>]... <OCTAL-MODE> <FILE>...
```

---

### 默认文件访问权限---Update

```sh {.line-numbers}
umask
```

1. `umask`仅对当前`shell`和子`shell`有效
1. 通常在`profile`或`rc`文件中运行`umask`

<!--TODO:
`umask`
-->

---

### 特殊访问权限

1. 仅对二进制可执行文件有效
    1. `s(suid, set-uid)`: 4
    1. `g(sgid, set-gid)`: 2
1. 仅对目录有效
    1. `t(sticky)`: 1

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨**Thank You**✨✨✨

🆗**End of This Slide**🆗
