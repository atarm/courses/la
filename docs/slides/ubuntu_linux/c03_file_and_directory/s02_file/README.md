---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_File_"
footer: "@aRoming"
math: katex
---

<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# File

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [文件结构](#文件结构)
2. [文件属性](#文件属性)
3. [文件类型](#文件类型)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 文件结构

---

1. `i-node`索引节点
1. 文件数据

>[阮一峰.理解inode](https://www.ruanyifeng.com/blog/2011/12/inode.html)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 文件属性

---

<!--TODO:
add it...
-->

1. 文件类型
1. 权限属性
1. `indoe`结点数
1. 文件大小
1. 文件所有者
1. 文件所属用户组
1. 时间属性：`atime(access time)`, `ctime(change time)`, `mtime(modified time)`
1. 文件名

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 文件类型

---

```sh {.line-numbers}
ls -l
```

---

1. 占用文件存储空间的文件类型
    1. `-`: regular file
    1. `d`: directory
    1. `l`: symbolic link
1. 不占用文件存储空间的文件类型（伪文件）
    1. `c` : character device file
    1. `b` : block device file
    1. `s` : local socket file
    1. `p` : named pipe

>[Identifying File types in Linux](https://linuxconfig.org/identifying-file-types-in-linux)

---
<!--end of slide page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

✨✨✨**Thank You**✨✨✨

🆗**End of This Slide**🆗
