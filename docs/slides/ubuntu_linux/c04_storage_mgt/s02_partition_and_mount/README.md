---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Partition and Mount_"
footer: "@aRoming"
math: katex
---

<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Partition and Mount分区与挂载

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Partition分区

---

1. `/boot`不能使用`lvm logical volume`
1. `/swap`不必独立分区，可不设置`swap`空间，也可使用`swapfile`

---

```bash {.line-numbers}
lsblk (8)            - list block devices
blkid (8)            - locate/print block device attributes
fdisk (8)            - manipulate disk partition table
parted (8)           - a partition manipulation program
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Make Filesystem创建文件系统

---

### 查看文件系统信息

```bash {.line-numbers}
file (1)             - determine file type
file -s, --special-files
```

---

### 创建文件系统

```bash {.line-numbers}
mkfs (8)             - build a Linux filesystem
mke2fs (8)           - create an ext2/ext3/ext4 filesystem
mkfs.ext4 (8)        - create an ext2/ext3/ext4 filesystem
```

---

### 卷标和`UUID`

```bash {.line-numbers}
mkfs.ext4 -L <new-volume-label>   # Set the volume label for the filesystem to new-volume-label.
e2label (8)          - Change the label on an ext2/ext3/ext4 filesystem
tune2fs (8)          - adjust tunable filesystem parameters on ext2/ext3/ext4 filesystems
tune2fs -L <volume-label>
tune2fs -U <UUID>
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 挂载文件系统

---

1. 一个文件系统不应被重复挂载到不同的目录中
1. 目录挂载新的文件系统后，原有内容将被隐藏
    1. 一个目录不应重复挂载多个文件系统
    1. 挂载点目录应为空目录

---

```bash {.line-numbers}
mount (8)            - mount a filesystem
mount -a, --all     # Mount all filesystems (of the given types) mentioned in fstab 
                    # (except for those whose line contains the noauto keyword).

umount (8)           - unmount file systems
```

---

```bash {.line-numbers}
# fstab (5)            - static information about the filesystems
# man fstab
fs_spec     fs_file     fs_vfstype      fs_mntops       fs_freq     fs_passno

cat /etc/mtab
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 文件系统查询和维护

---

### 文件系统查询

```bash {.line-numbers}
df (1)               - report file system disk space usage
du (1)               - estimate file space usage
```

---

### 文件系统修改和维护

```bash {.line-numbers}
fsck (8)             - check and repair a Linux filesystem
tune2fs (8)          - adjust tunable filesystem parameters on ext2/ext3/ext4 filesystems
tune2fs -j           # Add  an ext3 journal to the filesystem.
```

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨**Thank You**✨✨✨

🆗**End of This Slide**🆗
