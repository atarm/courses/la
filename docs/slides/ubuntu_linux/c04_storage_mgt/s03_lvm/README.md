---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Logical Volume Manager_"
footer: "@aRoming"
math: katex
---

<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Logical Volume Manager逻辑卷管理

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [LVM解决的问题](#lvm解决的问题)
2. [LVM的原理和术语](#lvm的原理和术语)
3. [`LVM`管理命令](#lvm管理命令)
4. [`LVM`注意事项](#lvm注意事项)
5. [`LVM`新增设备动态扩容已有文件系统](#lvm新增设备动态扩容已有文件系统)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## LVM解决的问题

---

### Problem of Physical Partition

1. 分区容量难于变化（可以扩充邻接的空闲空间）
1. 无法将不相邻的空闲空间（如，多个硬盘）合并到一个分区

### Advantage of LVM

1. 在线扩展或收缩分区空间
1. 跨物理空间组合分区空间

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## LVM的原理和术语

---

![height:600](./.assets/image/lvm.png)

>[File:Lvm.svg](https://commons.wikimedia.org/wiki/File:Lvm.svg)

---

![height:600](./.assets/image/LogicalVolumenManager.jpg)

>[Brief Introduction to Logical Volume Manager](https://www.brainupdaters.net/ca/brief-introduction-logical-volumes-lv-concept-example-application/)

---

1. `PV(Physical Volume, 物理卷)`：`PV`处于`LVM`系统最低层，它可以是整个硬盘，或者与磁盘分区具有相同功能的设备（如`RAID`），但和基本的物理存储介质相比较，多了与`LVM`相关管理参数
1. `VG(Volume Group, 卷组)`：创建在`PV`之上，由一个或多个`PV`组成，可以在`VG`上创建一个或多个`LV`，功能类似非`LVM`系统的物理硬盘
1. `LV(Logical Volume, 逻辑卷)`：从`VG`中分割出的一块空间，创建之后其大小可以伸缩，在`LV`上可以创建文件系统（如`/var`,`/home`）

>[逻辑卷管理器](https://zh.wikipedia.org/zh-cn/%E9%82%8F%E8%BC%AF%E6%8D%B2%E8%BB%B8%E7%AE%A1%E7%90%86%E5%93%A1)

---

1. `PE(Physical Extend, 物理区域)`：在创建`VG`时，将每一个`PV`被划分为基本单元（也被称为`PE`），具有唯一编号的`PE`是可以被`LVM`寻址的最小存储单元，默认为4MB
1. `LE(Logical Extend, 逻辑区域)`：`LV`的基本单元，是`PE`在`LV`上的映射
1. `LVM`通过控制`PE`实现调整文件系统的容量

---

1. `VG` <---> `disk`
1. `LV` <---> `partition`
1. `LE` <---> `block` ❓

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `LVM`管理命令

---

|功能|物理卷|卷组|逻辑卷|
|--|--|--|--|
|扫描|`pvscan`|`vgscan`|`lvscan`|
|显示基本信息|`pvs`|`vgs`|`lvs`|
|显示详细信息|`pvdisplay`|`vgdisplay`|`lvdisplay`|
|创建|`pvcreate`|`vgcreate`|`lvcreate`|
|删除|`pvremove`|`vgremove`|`lvremove`|
|扩充|`--`|`vgextend`|`lvextend/lvresize`|
|缩减|`--`|`vgreduce`|`lvreduce/lvresize`|
|改变属性|`pvchange`|`vgchange`|`lvchange`|

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `LVM`注意事项

---

1. `/boot`分区不能使用`LVM`，否则引导加载程序无法读取
1. `pvcreate`时，如果原有分区已存在文件系统，在转换成`PV`时将删除已有的文件系统

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `LVM`新增设备动态扩容已有文件系统

1. `fdisk`：对新设备创建物理分区（`id`配置为`8e`，新系统支持`83`创建`PV`）
1. `pvcreate`：将新物理分区转换成`PV`
1. `vgextend`：将新`PV`加入到`VG`
1. `lvextend`：扩容已有`FS`所在的`LV`
1. `resize2fs`/`xfs_growfs`：扩容已有`FS`

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨**Thank You**✨✨✨

🆗**End of This Slide**🆗
