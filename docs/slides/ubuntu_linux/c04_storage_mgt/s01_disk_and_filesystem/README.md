---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Disk and File System_"
footer: "@aRoming"
math: katex
---


<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Disk and File System

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Disk磁盘](#disk磁盘)
2. [Disk Device Naming磁盘设备命名](#disk-device-naming磁盘设备命名)
3. [磁盘分区](#磁盘分区)
4. [文件系统](#文件系统)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Disk磁盘

---

1. `disk`磁盘：通过磁记录技术来实现存储的存储设备
    1. `Floppy Disk`/`Soft Disk`，软磁盘（简称“软盘”）
    1. `Hard Disk`，硬磁盘（简称“硬盘”）
1. 分区==>格式化（建立文件系统）
1. 低级格式化：划分柱面、磁道、扇区（标识区、间隔区、数据区）❓
1. 高级格式化：建立文件系统、从逻辑上划分磁道

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Disk Device Naming磁盘设备命名

---

1. 字母顺序命名磁盘：同一类型接口下按字母顺序从`a`开始命名
1. 数字顺序命名分区：同一个磁盘下按数字顺序从`1`开始编号命名
1. `hd(hard disk)`：`IDE`
1. `sd(serial disk)`：`SCSI`、`SAS`、`SATA`、`USB`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 磁盘分区

---

1. 分区表类型：`MBR(Master Boot Record)`和`GPT(GUID Partition Table)`
1. `MBR`：至多4个分区、2TB容量
1. `GPT`：至多128个分区
1. `linux`分区编号从`1`到`16`（一个磁盘至多16个分区），主分区从`1`到`4`，逻辑分区从`5`到`16`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 文件系统

---

1. 主要文件系统：`ext2`, `ext3`, `ext4`
1. 其他文件系统：`xfs`, `hpfs`, `iso9660`, `minix`, `nfs`, `vfat(FAT16, FAT32, exFAT)`

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨**Thank You**✨✨✨

🆗**End of This Slide**🆗
